package com.rwd.listcrib.di.module

import com.rwd.listcrib.contract.*
import com.rwd.listcrib.presenter.*
import dagger.Module
import dagger.Provides


@Module
class FragmentModule {
    @Provides
    fun provideMainHomePresenter():MainHomeContract.Presenter{
        return MainHomePresenter()
    }
    @Provides
    fun provideMainSearchPresenter():MainSearchContract.Presenter{
        return MainSearchPresenter()
    }
    @Provides
    fun provideMainListPresenter():MainListContract.Presenter{
        return MainListPresenter()
    }
    @Provides
    fun provideMainFavouritesPresenter():MainFavouritesContract.Presenter{
        return MainFavouritesPresenter()
    }
    @Provides
    fun provideMainProfilePresenter():MainProfileContract.Presenter{
        return MainProfilePresenter()
    }

}