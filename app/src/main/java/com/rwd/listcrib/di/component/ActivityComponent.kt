package com.rwd.listcrib.di.component

import com.carveniche.wisdomleap.di.module.SharedPreferenceModule
import com.rwd.listcrib.di.PerApplication
import com.rwd.listcrib.di.module.ActivityModule
import com.rwd.listcrib.di.module.ContextModule
import com.rwd.listcrib.view.activity.*
import dagger.Component
import javax.inject.Singleton

@Singleton
@PerApplication
@Component(modules = [ActivityModule::class,ContextModule::class,SharedPreferenceModule::class])
interface ActivityComponent{
    fun inject(mainActivity: MainActivity)
    fun inject(splashActivity: SplashActivity)
    fun inject(loginActivity: LoginActivity)
    fun inject(addPropertyActivity: AddPropertyActivity)
    fun inject(listPropertyDetailActivity: ListPropertyDetailActivity)
    fun inject(editPropertyActivity: EditPropertyActivity)
    fun inject(otherViewsActivity: OtherViewsActivity)
    fun inject(chatUserActivity: ChatUserActivity)
    fun inject(chatReceiverActivity: ChatReceiverActivity)
    fun inject(editProfileActivity: EditProfileActivity)

}