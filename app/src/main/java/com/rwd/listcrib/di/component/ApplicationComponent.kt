package com.rwd.listcrib.di.component


import com.carveniche.wisdomleap.di.module.HttpModule
import com.carveniche.wisdomleap.di.module.RxThreadModule
import com.rwd.listcrib.BaseApp
import com.rwd.listcrib.di.module.ApplicationModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [HttpModule::class,
                        ApplicationModule::class,
                        RxThreadModule::class])
interface ApplicationComponent {
   fun inject(application : BaseApp)
}