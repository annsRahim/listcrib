package com.rwd.listcrib.di.component


import com.carveniche.wisdomleap.di.module.HttpModule
import com.carveniche.wisdomleap.di.module.SharedPreferenceModule

import com.rwd.listcrib.di.module.ContextModule
import com.rwd.listcrib.di.module.FragmentModule
import com.rwd.listcrib.view.fragments.*
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [FragmentModule::class, ContextModule::class,SharedPreferenceModule::class])
interface FragmentComponent {
    fun inject(mainHomeFragment: MainHomeFragment)
    fun inject(mainProfileFragment: MainProfileFragment)
    fun inject(mainFavouriteFragment: MainFavouriteFragment)
    fun inject(mainSearchFragment: MainSearchFragment)
    fun inject(mainListFragment: MainListFragment)
    fun inject(registerUserFragment: RegisterUserFragment)
    fun inject(loginFragment: LoginFragment)

}