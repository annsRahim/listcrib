package com.rwd.listcrib.di.module

import android.app.Activity
import android.content.Context
import com.rwd.listcrib.contract.MainContract
import com.rwd.listcrib.di.PerApplication
import com.rwd.listcrib.presenter.MainPresenter
import dagger.Module
import dagger.Provides
import javax.inject.Inject
import javax.inject.Singleton

@Module
class ActivityModule(private val activity: Activity)
{

    @PerApplication
    @Provides
    fun provideContext() : Context = activity


    @Provides
    fun provideMainPresenter() : MainContract.Presenter
    {
        return MainPresenter()
    }




}