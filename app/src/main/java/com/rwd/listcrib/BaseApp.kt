package com.rwd.listcrib

import android.app.Application
import com.carveniche.wisdomleap.di.module.HttpModule
import com.rwd.listcrib.di.component.ApplicationComponent
import com.rwd.listcrib.di.component.DaggerApplicationComponent
import com.rwd.listcrib.di.module.ApplicationModule


class BaseApp : Application() {

    lateinit var component: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        instance = this
        setup()
    }

    private fun setup() {
        component = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .httpModule(HttpModule())
            .build()
        component.inject(this)
    }

    fun getApplicationComponent(): ApplicationComponent {
        return component
    }

    companion object {
        lateinit var instance : BaseApp private set
    }



}