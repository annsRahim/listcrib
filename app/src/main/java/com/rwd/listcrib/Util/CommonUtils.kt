package com.rwd.listcrib.Util

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Context.LAYOUT_INFLATER_SERVICE
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Rect
import android.net.Uri
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.util.Patterns
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.widget.ShareDialog
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.rwd.listcrib.IPropertyShareListener
import com.rwd.listcrib.R
import com.rwd.listcrib.api.ApiInterface
import com.rwd.listcrib.view.activity.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.json.JSONArray
import pub.devrel.easypermissions.EasyPermissions
import java.io.File
import java.net.URLDecoder
import java.security.MessageDigest


fun isValidEmail(email : CharSequence) : Boolean{
    return Patterns.EMAIL_ADDRESS.toRegex().matches(email);
}
 fun isValidPhoneNumber(phone: String): Boolean {
    return if (phone.trim { it <= ' ' } != "" && phone.length > 9) {
        Patterns.PHONE.matcher(phone).matches()
    }
    else
        false
}
fun showLongToast(msg:String,context: Context)
{
    Toast.makeText(context,msg, Toast.LENGTH_LONG).show()
}
fun showShortToast(msg:String,context: Context)
{
    Toast.makeText(context,msg, Toast.LENGTH_SHORT).show()
}

fun showLoadingProgress(progressBar : ProgressBar,status : Boolean)
{
    progressBar.bringToFront()
    if(status)
        progressBar.visibility = View.VISIBLE
    else
        progressBar.visibility = View.GONE
}

fun showShareMenuPop(context: Context,ivFav : ImageView,listener : IPropertyShareListener,data : com.rwd.listcrib.models.propertListModel.Data){
    val inflater = context.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
    // Inflate the custom layout/view
    val customView = inflater.inflate(R.layout.pop_up_share_icons, null)
    val ivFaceBook = customView.findViewById<ImageView>(R.id.iv_facebook)
    ivFaceBook.setOnClickListener {
        listener.onItemSelected(1,data)
    }
    val ivTwitter = customView.findViewById<ImageView>(R.id.iv_twitter)
    val ivInstagram = customView.findViewById<ImageView>(R.id.iv_instagram)
    ivTwitter.setOnClickListener {
        listener.onItemSelected(2,data)
    }
    ivInstagram.setOnClickListener {
        listener.onItemSelected(3,data)
    }

    var popUpWindow = PopupWindow(customView,LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT)
    popUpWindow.isOutsideTouchable = true
    //popUpWindow.isFocusable = false
    var location =  locateView(ivFav)
    popUpWindow.animationStyle = R.style.sharePopUpAnimation
    popUpWindow.showAtLocation(ivFav, Gravity.TOP or Gravity.START, location!!.right, location.top)


}

fun locateView(v: View?): Rect? {
    val loc_int = IntArray(2)
    if (v == null) return null
    try {
        v!!.getLocationOnScreen(loc_int)
    } catch (npe: NullPointerException) {
        //Happens when the view doesn't exist on screen anymore.
        return null
    }

    val location = Rect()
    location.left = loc_int[0]
    location.top = loc_int[1]
    location.right = location.left + v!!.width
    location.bottom = location.top + v!!.height
    return location
}

fun goToListDetailActivity(context: Context,id : Int){
    var intent = Intent(context,ListPropertyDetailActivity::class.java)
    intent.putExtra(Constants.PROPERTY_ID,id)
    context.startActivity(intent)
}
fun goToMainActivity(context: Context){
    var intent = Intent(context,MainActivity::class.java)
    context.startActivity(intent)
}
fun goToLoginActivity(context: Context){
    var intent = Intent(context,LoginActivity::class.java)
    context.startActivity(intent)
}
fun goToAddPropertyActivity(context: Context){
    var intent = Intent(context,AddPropertyActivity::class.java)
    context.startActivity(intent)
}

fun toastErrorMessage(context: Context,error : List<String>){
    var msg = ""
    error.forEach {
        msg+= "$it \n"
    }
    showLongToast(msg,context)
}
fun getSharedPreference(context: Context): SharedPreferences? {
    return context.getSharedPreferences(Constants.MY_PREFS,Context.MODE_PRIVATE)
}

 fun hasCameraPermission(context: Context):Boolean {
    return EasyPermissions.hasPermissions(context, Manifest.permission.CAMERA)
}
 fun hasVideoAudioPermission(context: Context):Boolean {
    return EasyPermissions.hasPermissions(context, Manifest.permission.RECORD_AUDIO)
}
 fun hasWriteStoragePermission(context: Context):Boolean {
    return EasyPermissions.hasPermissions(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
}

fun getBitmapFromPath(path : String) : Bitmap{
    var imgFile = File(path)
    return BitmapFactory.decodeFile(imgFile.absolutePath)
}

fun goToOtherViewActivity(context: Context,viewType : String){
    var intent = Intent(context,OtherViewsActivity::class.java)
    intent.putExtra(Constants.OTHER_VIEW,viewType)
    context.startActivity(intent)

}

fun getFirstImageFromListingImage(listImages : String) : String{
    if(listImages.isNullOrEmpty())
        return Constants.SUBSTITUTE_IMAGE


    var listImage = listImages
    var jsonArray = JSONArray()
    listImage = URLDecoder.decode(listImage, "UTF-8");
    Log.d(Constants.LOG_TAG,"String images $listImage")
    try {
        jsonArray = JSONArray(listImage)
    }
    catch (ae : Exception){
        jsonArray.put(0,listImage)
    }
    return if(jsonArray.length()>0)
        jsonArray.getString(0)
    else
        Constants.SUBSTITUTE_IMAGE
}

 fun getRealPathFromURIPath(contentURI: Uri, activity: Activity): String? {
    val cursor: Cursor? = activity.contentResolver.query(contentURI, null, null, null, null)
    return if (cursor == null) {
        contentURI.getPath()
    } else {
        cursor.moveToFirst()
        val idx: Int = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
        cursor.getString(idx)
    }
}

fun printHashKey(context: Context){
    try{
        var info = context.packageManager.getPackageInfo(context.packageName, PackageManager.GET_SIGNATURES)
        info.signatures.forEach {
            var md = MessageDigest.getInstance("SHA")
            md.update(it.toByteArray())
            var hashkey = String(Base64.encode(md.digest(), 0));
            Log.d(Constants.LOG_TAG,"Hash Key "+hashkey)
        }


    }
    catch (ae: Exception){
        Log.d(Constants.LOG_TAG,"Hash Key failed "+ae.localizedMessage)
    }
}

fun hashFromSHA1(sha1: String) {
    val arr = sha1.split(":").toTypedArray()
    val byteArr = ByteArray(arr.size)
    for (i in arr.indices) {
        byteArr[i] = Integer.decode("0x" + arr[i]).toByte()
    }
    Log.d(
        Constants.LOG_TAG,
        Base64.encodeToString(byteArr, Base64.NO_WRAP)
    )
}

fun getFcmToken(apiInterface: ApiInterface){
    FirebaseInstanceId.getInstance().instanceId
        .addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w(Constants.LOG_TAG, "getInstanceId failed", task.exception)
                return@OnCompleteListener
            }

            // Get new Instance ID token
            val token = task.result?.token
            apiInterface.fcmUpdate(token!!).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                },{

                })

            Log.d(Constants.LOG_TAG, "FCM TOKEN : "+token)

        })
}

fun shareProperty(context: Context,application : String,title : String,description : String){


    when (application) {
        Constants.COM_TWITTER -> {

            var tweetUrl =  "https://twitter.com/intent/tweet?text=$title&url=$description";
            var uri = Uri.parse(tweetUrl)
            //var tweet =  Intent(Intent.ACTION_VIEW);
          //  tweet.setData(Uri.parse("http://twitter.com/?status=" + Uri.encode(description)));//where message is your string message
            context.startActivity(Intent(Intent.ACTION_VIEW, uri));
        }
        Constants.COM_FACEBOOK -> {

            if(ShareDialog.canShow(ShareLinkContent::class.java))
            {
                var linkContent = ShareLinkContent.Builder()
                    .setContentUrl(Uri.parse(description))
                    .build()
                var activity = context as Activity
                var shareDialog = ShareDialog(activity)
                shareDialog.show(linkContent)


            }

        }
        Constants.COM_INSTAGRAM -> {

//            var shareIntent = Intent(Intent.ACTION_SEND)
//            shareIntent.setType("image/*");
//            shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(des));
//            shareIntent.setPackage("com.instagram.android");

            try {
                var shareIntent =  Intent();
                shareIntent.action = Intent.ACTION_SEND
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(description));
                shareIntent.setType("image/*");
                context.startActivity(Intent.createChooser(shareIntent, "Share to"));
            }
            catch (ae : Exception)
            {
                Log.d(Constants.LOG_TAG,"Insta share error ${ae.localizedMessage}")
            }

        }
    }
}



