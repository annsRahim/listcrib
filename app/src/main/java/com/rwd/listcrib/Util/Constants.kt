package com.rwd.listcrib.Util

import android.content.Context
import android.util.Log

import io.reactivex.schedulers.Schedulers
import java.util.*

import com.google.android.gms.tasks.OnCompleteListener

import android.util.DisplayMetrics
import com.google.android.gms.common.util.Strings
import java.security.AccessController.getContext
import kotlin.math.roundToInt


class Constants {
    companion object {
        const val LOG_TAG = "ListCrib"
        const val MY_PREFS = "Prefs_listCrib_leap"

        const val IS_LOGGED = "IS_LOGGED"
        const val IS_SOCIAL_LOGIN = "IS_SOCIAL_LOGIN"
        const val USER_NAME = "USER_NAME"
        const val USER_CONTACT = "USER_CONTACT"
        const val USER_EMAIL = "USER_EMAIL"
        const val USER_ADDRESS = "USER_ADDRESS"
        const val USER_IMAGE = "USER_IMAGE"

        const val CHAT_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTS2E4ZGNjNTRlZTkwODYyNDcyNWRiN2Q4ODlhZjY3YTJkLTE1ODY0NTEwOTIiLCJpc3MiOiJTS2E4ZGNjNTRlZTkwODYyNDcyNWRiN2Q4ODlhZjY3YTJkIiwic3ViIjoiQUNjNTllMWMzN2ZmZjdiZmIyYTIxMzkyOGY3NTdjMzhkYyIsImV4cCI6MTU4NjQ1NDY5MiwiZ3JhbnRzIjp7ImlkZW50aXR5IjoiMTE3IiwiY2hhdCI6eyJzZXJ2aWNlX3NpZCI6IklTMTg0NTM4OWQ4ZmIxNDdjODk1NmY4OWZjNDE5NTg3ZGEifX19.ElwEUQgunfyoaTn0S7VPStpo_eoGOmNtOZq3WFGqrto"

        const val USER_FCM_TOKEN = "USER_FCM_TOKEN"
        const val USER_ID = "USER_ID"
        const val USER_TOKEN = "USER_TOKEN"
        const val SUBSTITUTE_IMAGE = "https://res.cloudinary.com/lucajax/image/upload/v1584975732/ixljb6mkl22dipw5icco.jpg"
        const val ERROR_RESPONSE = "Unable to fetch data"
        const val PROFILE_UPDATE_MESSAGE = "Profile Updated Successfully"
        const val SAMPLE_TOKEN = ""

        //Custom Font Path
        const val FONT_NICONNE = "fonts/Niconne-Regular.ttf"
        const val FONT_ROBOTO_LIGHT = "fonts/Roboto/Roboto-Light.ttf"

        const val FONT_AVENIR = "fonts/avenir/AvenirLTStd-Roman.otf"
        const val FONT_AVENIR_MEDIUM = "fonts/avenir/AvenirLTStd-Medium.otf"
        const val FONT_AVENIR_HEAVY = "fonts/avenir/AvenirLTStd-Heavy.otf"
        const val FONT_AVENIR_BLACK = "fonts/avenir/AvenirLTStd-Black.otf"

        const val SELECT = "Select"
        const val COMMERICIAL = "Commercial"
        const val RESIDENTIAL = "Residential"
        const val INDUSTRIAL = "Industrial"
        const val LAND = "Land"
        const val OTHERS = "Others"
        const val SALE = "Sale"
        const val LEASE = "Lease"
        const val SUB_LEASE = "SubLease"
        const val SOLD = "Sold"
        const val WANT = "Want"
        const val FAVOURITE_LIST = "FAVOURITE_LIST"
        const val CAMERA_VIDEO = "CAMERA_VIDEO"
        const val PROPERTY_ID = "PROPERTY_ID"
        const val CHANNEL_SID = "CHANNEL_SID"

        const val OTHER_VIEW = "OTHER_VIEW"
        const val PRIVACY_POLICY = "PRIVACY_POLICY"
        const val ABOUT_US = "ABOUT_US"
        const val CONTACT_US = "CONTACT_US"
        const val TERMS_CONDITIONS = "TERMS_CONDITIONS"
        const val CHAT_MESSAGES = "CHAT_MESSAGES"
        const val NOTIFICATIONS_LIST = "NOTIFICATIONS_LIST"
        const val MY_LISTINGS = "MY_LISTINGS"

        const val COM_FACEBOOK = "com.facebook.katana"
        const val COM_TWITTER = "com.twitter.android"
        const val COM_INSTAGRAM = "com.instagram.android"




         val LISTING_TYPE = arrayOf(SELECT, COMMERICIAL, RESIDENTIAL, INDUSTRIAL, LAND, OTHERS)
         val LISTING_USE = arrayOf(SELECT, SALE, LEASE, SUB_LEASE,SOLD, OTHERS)




    }
}