package com.rwd.listcrib.Util

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.rwd.listcrib.R
import com.rwd.listcrib.view.activity.MainActivity
import org.json.JSONException
import org.json.JSONObject


class FireBaseNotificationHandler : FirebaseMessagingService() {
    private var title = ""
    private var message = ""
    private var click_action = ""
    private var CHANNEL_ID = "com.rwd.listcrib.Util.Android"

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        // Check if message contains a data payload.
        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty()) {
            try {
                val data = JSONObject(remoteMessage.data)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }

        // Check if message contains a notification payload.
        // Check if message contains a notification payload.
        try {
            if (remoteMessage.notification != null) {
                title = remoteMessage.notification!!.title!! //get title
                message = remoteMessage.notification!!.body!! //get message
               // click_action = remoteMessage.notification!!.clickAction!! //get click_action
                Log.d(Constants.LOG_TAG, "Notification Title: $title")
                Log.d(Constants.LOG_TAG, "Notification Body: $message")
               // Log.d(Constants.LOG_TAG, "Notification click_action: $click_action")
                 sendNotification(title, message, click_action)
            }
        }
        catch (ae : Exception)
        {
            Log.e(Constants.LOG_TAG,"Notification Exception ${ae.localizedMessage}")
        }

    }

    private fun sendNotification(
        title: String,
        messageBody: String,
        click_action: String
    ) {
        val intent = Intent(this, MainActivity::class.java)
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
        val mBuilder= NotificationCompat.Builder (this, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_listcrib_new_logo_48dp)
            .setContentTitle(title)
            .setContentText(messageBody)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setContentIntent(pendingIntent) // Set the intent that will fire when the user taps the notification
            .setAutoCancel(true)
        val notificationManager: NotificationManagerCompat = NotificationManagerCompat.from(this)
        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(1, mBuilder.build())
        createNotificationChannel()
    }

    private fun createNotificationChannel() { // Create the NotificationChannel, but only on API 26+ because
// the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name: CharSequence = getString(R.string.app_name)
            val description = getString(R.string.app_description)
            val importance: Int = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(CHANNEL_ID, name, importance)
            channel.setDescription(description)
            // Register the channel with the system; you can't change the importance or other notification behaviors after this
            val notificationManager: NotificationManager = getSystemService<NotificationManager>(
                NotificationManager::class.java
            )
            notificationManager.createNotificationChannel(channel)
        }
    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
    }
}