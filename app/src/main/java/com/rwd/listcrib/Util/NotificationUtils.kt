package com.rwd.listcrib.Util

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.content.ContextCompat.getSystemService


fun createNotficationChannel(context : Context){
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val name: CharSequence ="PROPERTY"
        val description: String = "Property Updates"
        val notificationChannelID = "com.rwd.listcrib.Util.Android"
        val importance: Int = NotificationManager.IMPORTANCE_DEFAULT
        val channel = NotificationChannel(notificationChannelID, name, importance)
        channel.setDescription(description)
        // Register the channel with the system; you can't change the importance
// or other notification behaviors after this
        val notificationManager: NotificationManager =
            context.getSystemService(
                NotificationManager::class.java
            )
        notificationManager.createNotificationChannel(channel)
    }
}