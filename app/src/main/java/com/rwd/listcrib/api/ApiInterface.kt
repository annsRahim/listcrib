package com.rwd.listcrib.api


import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.facebook.login.Login


import com.rwd.listcrib.Util.Constants
import com.rwd.listcrib.Util.MySharedPreferences
import com.rwd.listcrib.Util.URL
import com.rwd.listcrib.Util.getSharedPreference
import com.rwd.listcrib.models.*

import com.rwd.listcrib.models.propertListModel.AllPropertyListModel
import io.reactivex.Observable
import io.reactivex.functions.Consumer
import io.reactivex.plugins.RxJavaPlugins

import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.io.IOException
import java.net.SocketTimeoutException
import java.util.logging.Handler
import io.reactivex.exceptions.UndeliverableException
import okhttp3.*
import java.net.SocketException
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.net.ssl.TrustManager


public interface ApiInterface {



    @FormUrlEncoded
    @POST("login")
    fun loginCheck(@Field("email") name: String,
                   @Field("password") password: String) : Observable<LoginUserModel>

    @FormUrlEncoded
    @POST("chatrequest")
    fun chatUserToken(@Field("property_id") propertyId: Int) : Observable<ChatTokenModel>

    @FormUrlEncoded
    @POST("gmailogincheck")
    fun gmailLoginCheck(@Field("email") email: String,
                        @Field("name") name: String,
                        @Field("id") id: String
                        ) : Observable<GoogleLoginModel>
    @FormUrlEncoded
    @POST("password")
    fun changePassword(@Field("email") email: String,
                        @Field("new_password") newPassword: String,
                        @Field("confirm_password") confirmPassword: String
    ) : Observable<BasicResponseModel>

    @FormUrlEncoded
    @POST("password/reset")
    fun forgotPassword(@Field("email") email: String
    ) : Observable<BasicResponseModel>

    @FormUrlEncoded
    @POST("fcmupdate")
    fun fcmUpdate(@Field("fcm") fcm: String
    ) : Observable<BasicResponseModel>

    @GET("listingproperty")
    fun listAllProperty() : Observable<AllPropertyListModel>

    @GET("listingallmy")
    fun myListingProperty() : Observable<AllPropertyListModel>

    @GET("chattoken")
    fun chatReceiverToken() : Observable<ChatReceiverTokenModel>

    @GET("feature_listing")
    fun listFeatureProperty(@Query("feature_listing") feature_listing : Int) : Observable<AllPropertyListModel>

    @GET("fav/all")
    fun listAllFavourites() : Observable<AllPropertyListModel>

    @GET("chatlist")
    fun getChatList() : Observable<ChatListModel>

    @GET("user")
    fun getUserProfile() : Observable<UserProfileModel>

    @GET("listing/{id}")
    fun propertyDetail(@Path("id") id: Int) : Observable<PropertyDetailModel>

    @GET("fav/add/{id}")
    fun addLidtFav(@Path("id") id: Int) : Observable<BasicResponseModel>

    @GET("fav/remove/{id}")
    fun removeLidtFav(@Path("id") id: Int) : Observable<BasicResponseModel>


//    @FormUrlEncoded
//    @POST("register")
//    fun addNewUser(@Field("name") name: String,
//                   @Field("password") password: String,
//                   @Field("email") email: String,
//                   @Field("contact") contact: String,
//                   @Field("address") address : String,
//                   @Field("city") city: String,
//                   @Field("state") state: String
//    ) : Observable<NewUserRegisterModel>

    @Multipart
    @POST("register")
    fun addNewUser( @Part("name") name : RequestBody,
                    @Part("password") password : RequestBody,
                    @Part("email") email : RequestBody,
                    @Part("contact") contact : RequestBody,
                    @Part("address") address : RequestBody,
                    @Part("city") city : RequestBody,
                    @Part("state") state: RequestBody
                    ) : Observable<NewUserRegisterModel>

    @Multipart
    @POST("register")
    fun addNewUserWithImage( @Part("name") name : RequestBody,
                    @Part("password") password : RequestBody,
                    @Part("email") email : RequestBody,
                    @Part("contact") contact : RequestBody,
                    @Part("address") address : RequestBody,
                    @Part("city") city : RequestBody,
                    @Part("state") state: RequestBody,
                             @Part f_image : MultipartBody.Part
    ) : Observable<NewUserRegisterModel>

    @Multipart
    @POST("userupdate")
    fun userUpdate( @Part("name") name : RequestBody,
                    @Part("email") email : RequestBody,
                    @Part("contact") contact : RequestBody,
                    @Part("user_address") address : RequestBody
    ) : Observable<NewUserRegisterModel>

    @Multipart
    @POST("userupdate")
    fun userUpdateWithImage( @Part("name") name : RequestBody,
                    @Part("email") email : RequestBody,
                    @Part("contact") contact : RequestBody,
                    @Part("user_address") address : RequestBody,
                    @Part f_image : MultipartBody.Part
    ) : Observable<NewUserRegisterModel>





    @Multipart
    @POST("propertystore")
    fun addProperty(
                        @Part("title") title : RequestBody,
                        @Part("price") price : RequestBody,
                        @Part("address") address : RequestBody,
                        @Part("listing_type") listing_type : RequestBody,
                        @Part("listing_use") listing_use : RequestBody,
                        @Part("listing_sqft") listing_sqft : RequestBody,
                        @Part("listing_beds") listing_beds : RequestBody,
                        @Part("listing_spot") listing_spot : RequestBody,
                        @Part("listing_baths") listing_baths : RequestBody,
                        @Part("listing_description") listing_description : RequestBody,
                        @Part property_image : List<MultipartBody.Part>,
                        @Part property_video : MultipartBody.Part

                    ): Observable<BasicResponseModel>


    @Multipart
    @POST("propertystore")
    fun addPropertyWithoutVideo(
        @Part("title") title : RequestBody,
        @Part("price") price : RequestBody,
        @Part("address") address : RequestBody,
        @Part("listing_type") listing_type : RequestBody,
        @Part("listing_use") listing_use : RequestBody,
        @Part("listing_sqft") listing_sqft : RequestBody,
        @Part("listing_beds") listing_beds : RequestBody,
        @Part("listing_spot") listing_spot : RequestBody,
        @Part("listing_baths") listing_baths : RequestBody,
        @Part("listing_description") listing_description : RequestBody,
        @Part property_image : List<MultipartBody.Part>

    ): Observable<BasicResponseModel>

    companion object {
        fun create():ApiInterface {

            var httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

            //   var okHttpClient = CustomOkHttpClient.getUnsafeOkHttpClient().newBuilder()
            var client = CustomOkHttpClient.getUnsafeOkHttpClient().newBuilder()
                .connectTimeout(5,TimeUnit.MINUTES)
                .writeTimeout(5,TimeUnit.MINUTES)
                .readTimeout(5,TimeUnit.MINUTES)
                .addInterceptor(httpLoggingInterceptor)
                .build()

            val retrofit = retrofit2.Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .baseUrl(URL.BASE_URL)
                .build()

//
            return retrofit.create(ApiInterface::class.java)
        }



        fun create(context:Context):ApiInterface {

            var httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

            var prefsContext = getSharedPreference(context)
            var userToken = prefsContext!!.getString(Constants.USER_TOKEN,"No TOken")
            Log.d(Constants.LOG_TAG,"User TOKEN : $userToken")
            Log.d(Constants.LOG_TAG,prefsContext.getString(Constants.USER_TOKEN,"No TOken"))

            var client =CustomOkHttpClient.getUnsafeOkHttpClient().newBuilder()
                .connectTimeout(5,TimeUnit.MINUTES)
                .writeTimeout(5,TimeUnit.MINUTES)
                .readTimeout(5,TimeUnit.MINUTES)
                .addInterceptor(httpLoggingInterceptor)
                .addInterceptor { chain ->
                    val newRequest = chain.request().newBuilder()
                        .addHeader("x-token",userToken)
                        .addHeader("Content-Type","application/json")
                        .build()
                    chain.proceed(newRequest)
                }
                .build()

            val retrofit = retrofit2.Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .baseUrl(URL.BASE_URL)
                .build()

//
            return retrofit.create(ApiInterface::class.java)
        }

    }

}

