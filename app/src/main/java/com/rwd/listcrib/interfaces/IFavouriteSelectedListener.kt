package com.rwd.listcrib.interfaces

interface IFavouriteSelectedListener {

    fun onFavouriteSelected(status : Boolean,propertyID : Int)
}