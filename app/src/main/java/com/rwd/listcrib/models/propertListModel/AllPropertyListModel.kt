package com.rwd.listcrib.models.propertListModel



data class AllPropertyListModel(
    val `data`: List<Data>,
    val status: Int
)

data class Data(
    val created_at: String,
    val feature_listing: Int,
    val id: Int,
    val image: String,
    val listing_address: String,
    val listing_baths_count: Int,
    val listing_beds_count: Int,
    val listing_description: String,
    val listing_image: String,
    val listing_parking_spot: Int,
    val listing_price: Int,
    val listing_sqft: String,
    val listing_title: String,
    val listing_type: String,
    val listing_use: String,
    val listing_video: String,
    val name: String,
    val updated_at: String,
    val user_address: String,
    val user_id: Int
)