package com.rwd.listcrib.models

data class ChatListModel(
    val fromchat: List<Fromchat>,
    val status: Int,
    val tochat: List<Tochat>
)

data class Fromchat(
    val channel: String,
    val created_at: String,
    val from_token: String,
    val from_user: String,
    val from_user_name: String,
    val id: Int,
    val propertys_id: String,
    val sid: String,
    val to_token: String,
    val to_user: String,
    val to_user_name: String,
    val tou_created_at: String,
    val tou_description: String,
    val tou_image: String,
    val tou_title: String,
    val tou_updated_at: String,
    val updated_at: String
)

data class Tochat(
    val channel: String,
    val created_at: String,
    val from_token: String,
    val from_user: String,
    val from_user_name: String,
    val id: Int,
    val propertys_id: String,
    val sid: String,
    val to_token: String,
    val to_user: String,
    val to_user_name: String,
    val tou_created_at: String,
    val tou_description: String,
    val tou_image: String,
    val tou_title: String,
    val tou_updated_at: String,
    val updated_at: String
)