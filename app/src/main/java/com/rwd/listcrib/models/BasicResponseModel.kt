package com.rwd.listcrib.models

data class BasicResponseModel(
    val msg: String,
    val status: Int
)