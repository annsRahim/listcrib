package com.rwd.listcrib.models

data class NewPropertyListModel(
    val name: String,
    val listing_type: String,
    val listing_price: String,
    val listing_address: String,
    val listing_image: String
)
