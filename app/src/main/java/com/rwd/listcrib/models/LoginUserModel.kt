package com.rwd.listcrib.models

data class LoginUserModel(
    val msg: String,
    val role: String,
    val status: Int,
    val token: String,
    val userid : Int,
    val errors: List<String>
)