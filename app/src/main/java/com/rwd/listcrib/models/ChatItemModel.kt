package com.rwd.listcrib.models



data class ChatItemModel(

    val from_user_name: String,
    val to_user_name: String,
    val tou_image: String,
    val sid: String,
    val updated_at: String
)

