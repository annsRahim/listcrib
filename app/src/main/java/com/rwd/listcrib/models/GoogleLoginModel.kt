package com.rwd.listcrib.models

data class GoogleLoginModel(
    val msg: String,
    val role: String,
    val status: Int,
    val token: String,
    val userid: Int
)