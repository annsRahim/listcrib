package com.rwd.listcrib.models

data class UserProfileModel(
    val city: String,
    val contact: String,
    val created_at: String,
    val email: String,
    val facebook_id: String,
    val fcm_token: String,
    val google_id: String,
    val id: Int,
    val image: String,
    val is_delete: Int,
    val name: String,
    val state: String,
    val status: Int,
    val updated_at: String,
    val user_address: String,
    val errors: List<String>
)