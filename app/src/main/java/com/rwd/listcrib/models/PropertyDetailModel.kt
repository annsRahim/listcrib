package com.rwd.listcrib.models

data class PropertyDetailModel(
    val `data`: DetailData,
    val status: Int,
    val errors: List<String>
)

data class DetailData(
    val created_at: String,
    val feature_listing: Int,
    val image: String,
    val listing_address: String,
    val listing_baths_count: Int,
    val listing_beds_count: Int,
    val listing_description: String,
    val listing_image: String,
    val listing_parking_spot: Int,
    val listing_price: Int,
    val listing_sqft: String,
    val listing_title: String,
    val listing_type: String,
    val listing_use: String,
    val listing_video: String,
    val name: String,
    val updated_at: String,
    val user_address: String,
    val user_id: Int
)
