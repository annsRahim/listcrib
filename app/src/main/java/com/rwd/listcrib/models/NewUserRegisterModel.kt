package com.rwd.listcrib.models


data class NewUserRegisterModel(
    val `data`: Data,
    val msg: String,
    val role: String,
    val status: Int,
    val token: String,
    val errors: List<String>
)

data class Data(
    val city: String,
    val contact: String,
    val created_at: String,
    val email: String,
    val fcm_token: Any,
    val id: Int,
    val image: String,
    val name: String,
    val state: String,
    val status: Int,
    val updated_at: String,
    val user_address: String
)