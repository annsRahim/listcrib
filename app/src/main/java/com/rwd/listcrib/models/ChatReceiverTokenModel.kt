package com.rwd.listcrib.models

data class ChatReceiverTokenModel(
    val token: String
)