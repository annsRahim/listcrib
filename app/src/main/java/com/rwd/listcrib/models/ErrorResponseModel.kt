package com.rwd.listcrib.models

data class ErrorResponseModel(
    val errors: List<String>
)