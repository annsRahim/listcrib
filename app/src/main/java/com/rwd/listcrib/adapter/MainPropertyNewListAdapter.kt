package com.rwd.listcrib.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rwd.listcrib.R
import com.rwd.listcrib.Util.Constants
import com.rwd.listcrib.Util.getFirstImageFromListingImage
import com.rwd.listcrib.Util.goToListDetailActivity
import com.rwd.listcrib.Util.shareProperty
import com.rwd.listcrib.models.NewPropertyListModel
import com.rwd.listcrib.models.propertListModel.Data
import com.squareup.picasso.Picasso


class MainPropertyNewListAdapter(private var context : Context, private var propertList : List<Data>) : RecyclerView.Adapter<MainPropertyNewListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_new_property_list_main,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return propertList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(propertList[position],context)
    }

    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(data : Data,context: Context)
        {
            val tvPropertyName  = itemView.findViewById<TextView>(R.id.tv_property_name)
            val tvPropertyPrice  = itemView.findViewById<TextView>(R.id.tv_property_price)
            val tvPropertyAddress  = itemView.findViewById<TextView>(R.id.tv_property_address)
            val ivPropertyImage = itemView.findViewById<ImageView>(R.id.iv_property)

            tvPropertyName.text = "Property on \n"+data.listing_use
            tvPropertyName.visibility = View.GONE
            tvPropertyAddress.text = data.listing_address
            tvPropertyPrice.text = data.listing_price.toString()

            var firstImage = Constants.SUBSTITUTE_IMAGE
            if(!data.listing_image.isNullOrEmpty())
                firstImage = getFirstImageFromListingImage(data.listing_image)
            Picasso.with(context).load(firstImage)
                .into(ivPropertyImage)

            ivPropertyImage.setOnClickListener {

                goToListDetailActivity(context,data.id)
            }




//            tvQuizName.text = data.chapter_name.replace("_"," ")
//            tvQuizDate.text = data.date
//            tvQuizScore.text = "Score : ${data.score}"
//            tvQuizTotal.text = "Total : ${data.total}"


        }

    }
}