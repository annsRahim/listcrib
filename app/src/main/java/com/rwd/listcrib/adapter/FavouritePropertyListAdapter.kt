package com.rwd.listcrib.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.rwd.listcrib.IPropertyShareListener
import com.rwd.listcrib.R
import com.rwd.listcrib.Util.Constants
import com.rwd.listcrib.Util.goToListDetailActivity
import com.rwd.listcrib.Util.showShareMenuPop
import com.rwd.listcrib.Util.showShortToast
import com.rwd.listcrib.models.propertListModel.Data
import com.squareup.picasso.Picasso


class FavouritePropertyListAdapter(private var context: Context, private var propertList : List<Data>) : RecyclerView.Adapter<FavouritePropertyListAdapter.ViewHolder>() {
 lateinit var propertyListFiltered : List<Data>
    init {
        Log.d(Constants.LOG_TAG,"SSSSS HHHHH KKKKK")
        this.propertyListFiltered = propertList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_search_property_list,parent,false)
        return ViewHolder(view,context)
    }

    override fun getItemCount(): Int {

        return propertList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(propertList[position])
    }

    class ViewHolder(itemView : View,var context: Context) : RecyclerView.ViewHolder(itemView) {


        fun bindItems(listData : Data)
        {
            val tvUserName  = itemView.findViewById<TextView>(R.id.tv_user_name)
            val tvUserLocation  = itemView.findViewById<TextView>(R.id.tv_user_location)
            val tvPropertyName  = itemView.findViewById<TextView>(R.id.tv_property_name)
            val tvPropertyPrice  = itemView.findViewById<TextView>(R.id.tv_property_price)
            val tvPropertyAddress  = itemView.findViewById<TextView>(R.id.tv_property_address)
            val tvSqft  = itemView.findViewById<TextView>(R.id.tv_sqft)
            val tvParking  = itemView.findViewById<TextView>(R.id.tv_parking)
            val tvBath  = itemView.findViewById<TextView>(R.id.tv_bath)
            val tvBeds  = itemView.findViewById<TextView>(R.id.tv_beds)


            val ivProperty = itemView.findViewById<ImageView>(R.id.iv_property)
            val ivAvatar = itemView.findViewById<ImageView>(R.id.iv_avatar)

            tvUserName.text = listData.name
            tvUserLocation.text = listData.user_address

            if(!listData.listing_image.isNullOrEmpty())
                Picasso.with(context).load(listData.listing_image)
                    .into(ivProperty)
            else
            {
                Picasso.with(context).load(Constants.SUBSTITUTE_IMAGE)
                    .into(ivProperty)
            }

            if(!listData.image.isNullOrEmpty())
                Picasso.with(context).load(listData.image)
                    .into(ivAvatar)

            ivProperty.setOnClickListener {
                goToListDetailActivity(context!!,listData.id)
            }
//            Picasso.with(context).load("https://media.gettyimages.com/photos/idyllic-home-with-covered-porch-picture-id479767332?s=612x612")
//                .into(ivProperty)
            tvPropertyName.text = listData.name
            tvPropertyAddress.text = listData.listing_address
            tvPropertyPrice.text = listData.listing_price.toString()
            tvBath.text = context.getString(R.string.bath_count,listData.listing_baths_count)
            tvBeds.text = context.getString(R.string.bed_count,listData.listing_beds_count)
            tvParking.text = context.getString(R.string.parking_count,listData.listing_parking_spot)
            tvSqft.text = context.getString(R.string.sqft_count,listData.listing_sqft)
//            tvQuizName.text = data.chapter_name.replace("_"," ")
//            tvQuizDate.text = data.date
//            tvQuizScore.text = "Score : ${data.score}"
//            tvQuizTotal.text = "Total : ${data.total}"


        }

//        override fun onItemSelected(item: Int) {
//            showShortToast("Item Selected $item",context)
//        }


    }



}