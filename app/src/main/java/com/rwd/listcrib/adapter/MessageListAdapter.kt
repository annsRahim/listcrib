package com.rwd.listcrib.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.rwd.listcrib.IPropertyShareListener
import com.rwd.listcrib.R
import com.rwd.listcrib.Util.Constants
import com.rwd.listcrib.Util.goToListDetailActivity
import com.rwd.listcrib.Util.showShareMenuPop
import com.rwd.listcrib.Util.showShortToast
import com.rwd.listcrib.models.propertListModel.Data
import com.squareup.picasso.Picasso
import com.twilio.chat.Message


class MessageListAdapter(private var context: Context,private var userID : String,private var messageList : List<Message>) : RecyclerView.Adapter<MessageListAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_message_list,parent,false)
        return ViewHolder(view,context)
    }

    override fun getItemCount(): Int {

        return messageList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(messageList[position],userID)
    }

    class ViewHolder(itemView : View,var context: Context) : RecyclerView.ViewHolder(itemView) {


        fun bindItems(listMessage : Message,userID: String)
        {

            val tvSenderMessageText  = itemView.findViewById<TextView>(R.id.tv_sender_message)
            val tvReceiverMessageText  = itemView.findViewById<TextView>(R.id.tv_receiver_message)
            if(userID == listMessage.author)
            {
                tvSenderMessageText.text = listMessage.messageBody
                tvSenderMessageText.visibility = View.VISIBLE
                tvReceiverMessageText.visibility = View.GONE
            }
            else
            {
                tvReceiverMessageText.text = listMessage.messageBody
                tvSenderMessageText.visibility = View.GONE
                tvReceiverMessageText.visibility = View.VISIBLE
            }


        }




    }



}