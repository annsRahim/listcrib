package com.rwd.listcrib.adapter;

import android.content.Context;
import android.util.Log;
import android.view.ContextMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.rwd.listcrib.R;
import com.rwd.listcrib.Util.Constants;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;

public class ImageGalleryAdapter extends PagerAdapter {

    Context mContext;
    JSONArray mImageArray;

    public ImageGalleryAdapter(Context context, JSONArray imageArray){
        this.mContext = context;
        this.mImageArray = imageArray;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ImageView) object);
    }

    private int[] sliderImageId = new int[]{
            R.drawable.ic_instagram, R.drawable.ic_facebook, R.drawable.ic_photo_gallery,R.drawable.ic_sample_man, R.drawable.ic_video_play,
    };

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = new ImageView(mContext);
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        try {
            Picasso.with(mContext).load(mImageArray.get(position).toString())
                    .into(imageView);
            Log.d(Constants.LOG_TAG,mImageArray.get(position).toString()+"--"+position);
        } catch (JSONException e) {
           Log.d(Constants.LOG_TAG,"JSON EXCEPTION "+e.getLocalizedMessage());
        }
       // imageView.setImageResource(sliderImageId[position]);
        ((ViewPager) container).addView(imageView, 0);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((ImageView) object);
    }

    @Override
    public int getCount() {
        return mImageArray.length();
    }
}
