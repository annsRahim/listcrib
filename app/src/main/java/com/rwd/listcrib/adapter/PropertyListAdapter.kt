package com.rwd.listcrib.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rwd.listcrib.IPropertyShareListener
import com.rwd.listcrib.R
import com.rwd.listcrib.Util.*
import com.rwd.listcrib.interfaces.IFavouriteSelectedListener
import com.rwd.listcrib.models.propertListModel.Data
import com.squareup.picasso.Picasso
import java.util.*


class PropertyListAdapter(private var context: Context,var propertList : List<Data>,var listener : IFavouriteSelectedListener,var mActivity : Activity) : RecyclerView.Adapter<PropertyListAdapter.ViewHolder>() {


    init {

        Collections.reverse(propertList)

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_property_list,parent,false)
        return ViewHolder(view,context)
    }

    override fun getItemCount(): Int {

        return propertList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(propertList[position],listener)
    }

    class ViewHolder(itemView : View,var context: Context) : RecyclerView.ViewHolder(itemView),IPropertyShareListener
    {


        fun bindItems(listData : Data,listener: IFavouriteSelectedListener)
        {

            val tvUserName  = itemView.findViewById<TextView>(R.id.tv_user_name)
            val tvUserLocation  = itemView.findViewById<TextView>(R.id.tv_user_location)
            val tvPropertyName  = itemView.findViewById<TextView>(R.id.tv_property_name)
            val tvPropertyPrice  = itemView.findViewById<TextView>(R.id.tv_property_price)
            val tvPropertyAddress  = itemView.findViewById<TextView>(R.id.tv_property_address)
            val tvSqft  = itemView.findViewById<TextView>(R.id.tv_sqft)
            val tvParking  = itemView.findViewById<TextView>(R.id.tv_parking)
            val tvBath  = itemView.findViewById<TextView>(R.id.tv_bath)
            val tvBeds  = itemView.findViewById<TextView>(R.id.tv_beds)
            val ivFav = itemView.findViewById<ImageView>(R.id.iv_fav)
            var regrexIDCHECK = "+${listData.id}+"
            var favouriteList = getSharedPreference(context)!!.getString(Constants.FAVOURITE_LIST,"");
            var isFavAdded = favouriteList.contains(regrexIDCHECK)

            var firstImage = Constants.SUBSTITUTE_IMAGE
            if(!listData.listing_image.isNullOrEmpty())
                firstImage = getFirstImageFromListingImage(listData.listing_image)

            if(isFavAdded)
                ivFav.setBackgroundResource(R.drawable.ic_favorite_red_24dp)
            else
                ivFav.setBackgroundResource(R.drawable.ic_favorite_border_red_24dp)

            ivFav.setOnClickListener {
                var lc_favouriteList = getSharedPreference(context)!!.getString(Constants.FAVOURITE_LIST,"");
                var lc_regrexIDCHECK = "+${listData.id}+"
                var lc_isFavAdded = lc_favouriteList.contains(lc_regrexIDCHECK)

                if(lc_isFavAdded)
                {
                    ivFav.setBackgroundResource(R.drawable.ic_favorite_border_red_24dp)
                    listener.onFavouriteSelected(false,listData.id)
                }
                else{
                    ivFav.setBackgroundResource(R.drawable.ic_favorite_red_24dp)
                    listener.onFavouriteSelected(true,listData.id)
                }

            }
            val ivSend = itemView.findViewById<ImageView>(R.id.iv_send)
            val ivProperty = itemView.findViewById<ImageView>(R.id.iv_property)
            ivProperty.setOnClickListener {
                goToListDetailActivity(context!!,listData.id)
            }
            val ivAvatar = itemView.findViewById<ImageView>(R.id.iv_avatar)
            ivSend.setOnClickListener {
                showShareMenuPop(context,ivSend,this,listData)
            }
            Picasso.with(context).load(firstImage)
                .into(ivProperty)
//            if(!listData.listing_image.isNullOrEmpty())
//                Picasso.with(context).load(listData.listing_image)
//                    .into(ivProperty)
//            else
//            {
//                Picasso.with(context).load(Constants.SUBSTITUTE_IMAGE)
//                    .into(ivProperty)
//            }
            if(!listData.image.isNullOrEmpty())
                Picasso.with(context).load(listData.image)
                    .into(ivAvatar)

            tvUserName.text = listData.name
            if(listData.user_address.isNullOrEmpty())
                tvUserLocation.text = ""
            else
                tvUserLocation.text = listData.user_address

//            Picasso.with(context).load("https://media.gettyimages.com/photos/idyllic-home-with-covered-porch-picture-id479767332?s=612x612")
//                .into(ivProperty)
            tvPropertyName.text = listData.name
            tvPropertyAddress.text = listData.listing_address
            tvPropertyPrice.text = listData.listing_price.toString()
            tvBath.text = context.getString(R.string.bath_count,listData.listing_baths_count)
            tvBeds.text = context.getString(R.string.bed_count,listData.listing_beds_count)
            tvParking.text = context.getString(R.string.parking_count,listData.listing_parking_spot)
            tvSqft.text = context.getString(R.string.sqft_count,listData.listing_sqft)
//            tvQuizName.text = data.chapter_name.replace("_"," ")
//            tvQuizDate.text = data.date
//            tvQuizScore.text = "Score : ${data.score}"
//            tvQuizTotal.text = "Total : ${data.total}"


        }

        override fun onItemSelected(item: Int,data : Data) {
           //
            var firstImage = Constants.SUBSTITUTE_IMAGE
            if(!data.listing_image.isNullOrEmpty())
                firstImage = getFirstImageFromListingImage(data.listing_image)
            when (item) {
                1 -> {
                    shareProperty(context,Constants.COM_FACEBOOK,data.listing_title,firstImage)
                }
                2 -> {
                    shareProperty(context,Constants.COM_TWITTER,data.listing_title,firstImage)
                }
                else -> {
                    shareProperty(context,Constants.COM_INSTAGRAM,data.listing_title,firstImage)
                }
            }
            //showShortToast("Item Selected $item",context)
        }




    }


}