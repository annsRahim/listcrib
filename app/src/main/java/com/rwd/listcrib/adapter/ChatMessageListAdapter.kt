package com.rwd.listcrib.adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rwd.listcrib.R
import com.rwd.listcrib.Util.Constants
import com.rwd.listcrib.Util.getFirstImageFromListingImage
import com.rwd.listcrib.Util.goToListDetailActivity
import com.rwd.listcrib.models.ChatItemModel
import com.rwd.listcrib.models.Fromchat
import com.rwd.listcrib.models.NewPropertyListModel
import com.rwd.listcrib.models.Tochat
import com.rwd.listcrib.models.propertListModel.Data
import com.rwd.listcrib.view.activity.ChatReceiverActivity
import com.squareup.picasso.Picasso


class ChatMessageListAdapter(private var context : Context, private var messageList : MutableList<ChatItemModel>) : RecyclerView.Adapter<ChatMessageListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_chat_message_list,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return messageList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(messageList[position],context)
    }

    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(data : ChatItemModel,context: Context)
        {
            val tvPropertyName  = itemView.findViewById<TextView>(R.id.tv_property_name)
            val tvPropertyUserName = itemView.findViewById<TextView>(R.id.tv_property_user_name)
            val tvPropertyTime  = itemView.findViewById<TextView>(R.id.tv_property_time)
            val ivPropertyImage = itemView.findViewById<ImageView>(R.id.iv_property)
            val llChatListContainer = itemView.findViewById<LinearLayout>(R.id.ll_chat_list_container)

            tvPropertyName.text = data.from_user_name
            tvPropertyUserName.text = data.to_user_name
            tvPropertyTime.text = data.updated_at


            var firstImage = Constants.SUBSTITUTE_IMAGE
            if(!data.tou_image.isNullOrEmpty())
                firstImage = getFirstImageFromListingImage(data.tou_image)
            Picasso.with(context).load(firstImage)
                .into(ivPropertyImage)

            llChatListContainer.setOnClickListener {
                var intent = Intent(context,ChatReceiverActivity::class.java)
                intent.putExtra(Constants.CHANNEL_SID,data.sid)
                context.startActivity(intent)
            }




        }

    }
}