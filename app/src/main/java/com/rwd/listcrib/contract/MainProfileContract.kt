package com.rwd.listcrib.contract

import com.rwd.listcrib.base.BaseContract

class MainProfileContract {
    interface View: BaseContract.View{

    }
    interface Presenter : BaseContract.Presenter<View>{
        
    }
}