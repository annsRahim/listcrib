package com.rwd.listcrib.contract

import com.rwd.listcrib.base.BaseContract
import com.rwd.listcrib.models.propertListModel.AllPropertyListModel

class MainHomeContract {
    interface View: BaseContract.View{
        fun loadFeatureListDatas(propertyListModel: AllPropertyListModel)
        fun loadFeatureListFailed(errorMsg : String)
        fun loadNewPropertyListSuccess(propertyListModel: AllPropertyListModel)
        fun loadNewPropertyListFailed(errorMsg: String)
    }
    interface Presenter : BaseContract.Presenter<View>{
        fun loadFeatureList()
        fun loadNewPropertyList()
    }
}