package com.rwd.listcrib.contract

import com.rwd.listcrib.base.BaseContract

class MainContract {
    interface View : BaseContract.View{

    }
    interface Presenter : BaseContract.Presenter<View>{

    }
}