package com.rwd.listcrib.contract

import com.rwd.listcrib.base.BaseContract
import com.rwd.listcrib.models.propertListModel.AllPropertyListModel

class MainSearchContract {
    interface View: BaseContract.View{
        fun onListLoadDataSuccess(propertyListModel: AllPropertyListModel)
        fun onListLoadDataFailed(msg : String)

    }
    interface Presenter : BaseContract.Presenter<View>{
        fun loadPropertyList()
    }
}