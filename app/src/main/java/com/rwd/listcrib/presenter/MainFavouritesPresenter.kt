package com.rwd.listcrib.presenter

import android.util.Log
import com.rwd.listcrib.Util.Constants
import com.rwd.listcrib.api.ApiInterface
import com.rwd.listcrib.contract.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainFavouritesPresenter : MainFavouritesContract.Presenter{

    private val api = ApiInterface.create()
    lateinit var view : MainFavouritesContract.View
    var disposable = CompositeDisposable()

    override fun loadPropertyList() {
        var listDisposable = api.listAllFavourites()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Log.d(Constants.LOG_TAG,"Favourites RRRR")
                Log.d(Constants.LOG_TAG,it.toString())
                view.onListLoadDataSuccess(it)
            },{
                Log.d(Constants.LOG_TAG,it.localizedMessage)
                view.onListLoadDataFailed(it.localizedMessage)
            })
        disposable.add(listDisposable)
    }

    override fun subscribe() {

    }

    override fun unSubscribe() {
        disposable.clear()
    }

    override fun attach(view: MainFavouritesContract.View) {
        this.view = view
    }

}