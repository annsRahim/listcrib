package com.rwd.listcrib.presenter

import android.util.Log
import com.rwd.listcrib.Util.Constants
import com.rwd.listcrib.api.ApiInterface
import com.rwd.listcrib.contract.MainContract
import com.rwd.listcrib.contract.MainHomeContract
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainHomePresenter : MainHomeContract.Presenter{

    private val api = ApiInterface.create()
    lateinit var view : MainHomeContract.View
    var disposable = CompositeDisposable()


    override fun loadFeatureList() {
        view.showProgress(true)
        var listDisposable = api.listFeatureProperty(1)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                view.showProgress(false)
                view.loadFeatureListDatas(it)
            },{
                view.showProgress(false)
                Log.d(Constants.LOG_TAG,it.localizedMessage)
                view.loadFeatureListFailed(it.localizedMessage)
            })
        disposable.add(listDisposable)
    }

    override fun loadNewPropertyList() {

        var listNewDisposable = api.listAllProperty()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

                view.loadNewPropertyListSuccess(it)
            },{
                view.showProgress(false)

                view.loadNewPropertyListFailed(it.localizedMessage)
            })
        disposable.add(listNewDisposable)
    }

    override fun subscribe() {

    }

    override fun unSubscribe() {
        disposable.clear()
    }

    override fun attach(view: MainHomeContract.View) {
        this.view = view
    }

}