package com.rwd.listcrib.presenter

import android.util.Log
import android.view.View
import com.rwd.listcrib.Util.Constants
import com.rwd.listcrib.api.ApiInterface
import com.rwd.listcrib.contract.MainContract
import com.rwd.listcrib.contract.MainHomeContract
import com.rwd.listcrib.contract.MainSearchContract
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainSearchPresenter : MainSearchContract.Presenter{

    private val api = ApiInterface.create()
    lateinit var view : MainSearchContract.View
    var disposable = CompositeDisposable()



    override fun subscribe() {

    }

    override fun loadPropertyList() {
        var listDisposable = api.listAllProperty()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

                view.onListLoadDataSuccess(it)
            },{
                Log.d(Constants.LOG_TAG,it.localizedMessage)
                view.onListLoadDataFailed(it.localizedMessage)
            })
        disposable.add(listDisposable)
    }

    override fun unSubscribe() {
        disposable.clear()
    }

    override fun attach(view: MainSearchContract.View) {
        this.view = view
    }

}