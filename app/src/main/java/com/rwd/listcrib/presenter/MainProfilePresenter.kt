package com.rwd.listcrib.presenter

import com.rwd.listcrib.api.ApiInterface
import com.rwd.listcrib.contract.*
import io.reactivex.disposables.CompositeDisposable

class MainProfilePresenter : MainProfileContract.Presenter{

    private val api = ApiInterface.create()
    lateinit var view : MainProfileContract.View
    var disposable = CompositeDisposable()

    override fun subscribe() {

    }

    override fun unSubscribe() {
        disposable.clear()
    }

    override fun attach(view: MainProfileContract.View) {
        this.view = view
    }

}