package com.rwd.listcrib.presenter

import android.util.Log
import android.view.View
import com.rwd.listcrib.Util.Constants
import com.rwd.listcrib.api.ApiInterface
import com.rwd.listcrib.contract.MainContract
import com.rwd.listcrib.contract.MainHomeContract
import com.rwd.listcrib.contract.MainListContract
import com.rwd.listcrib.contract.MainSearchContract
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainListPresenter : MainListContract.Presenter{


    private val api = ApiInterface.create()
    lateinit var view : MainListContract.View
    var disposable = CompositeDisposable()
    private lateinit var rootView: View

    override fun subscribe() {

    }

    override fun unSubscribe() {
        disposable.clear()
    }

    override fun attach(view: MainListContract.View) {
        this.view = view
    }

    override fun loadPropertyList() {
        var listDisposable = api.listAllProperty()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Log.d(Constants.LOG_TAG,it.toString())
                view.onListLoadDataSuccess(it)
            },{
                Log.d(Constants.LOG_TAG,it.localizedMessage)
                view.onListLoadDataFailed(it.localizedMessage)
            })
        disposable.add(listDisposable)
    }

}