package com.rwd.listcrib.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import com.carveniche.wisdomleap.di.module.SharedPreferenceModule
import com.rwd.listcrib.R
import com.rwd.listcrib.Util.*
import com.rwd.listcrib.di.component.DaggerActivityComponent
import com.rwd.listcrib.di.module.ActivityModule
import com.rwd.listcrib.di.module.ContextModule
import com.rwd.listcrib.view.fragments.*
import javax.inject.Inject

class OtherViewsActivity : AppCompatActivity() {
    @Inject
    lateinit var mySharedPreferences: MySharedPreferences

    private var selectedView = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_other_views)
        injectDependency()
        selectedView = intent.getStringExtra(Constants.OTHER_VIEW)
        showSelectedView()
        customActionBar()

    }
    private fun customActionBar() {
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        supportActionBar!!.setDisplayShowCustomEnabled(true);
        supportActionBar!!.setCustomView(R.layout.action_bar_view);
        var view = supportActionBar!!.customView
        var ivActionBack = view.findViewById<ImageView>(R.id.iv_action_back)
        ivActionBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun showSelectedView() {
        when(selectedView){
            Constants.PRIVACY_POLICY->showPrivacyPolicyFragment()
            Constants.ABOUT_US->showAboutUsFragment()
            Constants.CONTACT_US->showContactUsFragment()
            Constants.TERMS_CONDITIONS->showTermsAndConditionsFragment()
            Constants.CHAT_MESSAGES->showChatMessageFragment()
            Constants.NOTIFICATIONS_LIST->showNotificationListFragment()
            Constants.MY_LISTINGS->showMyLisitngsFragment()
        }
    }

    private fun showNotificationListFragment() {
        supportFragmentManager.beginTransaction()
            .addToBackStack(null)
            .replace(R.id.frame,NotificationListFragment(),NotificationListFragment.TAG)
            .commit()
    }
    private fun showAboutUsFragment() {
        supportFragmentManager.beginTransaction()
            .addToBackStack(null)
            .replace(R.id.frame,AboutUsFragment(),"")
            .commit()
    }

    private fun showContactUsFragment(){
        supportFragmentManager.beginTransaction()
            .addToBackStack(null)
            .replace(R.id.frame,ContactUsFragment(),"")
            .commit()
    }
    private fun showChatMessageFragment(){
        supportFragmentManager.beginTransaction()
            .addToBackStack(null)
            .replace(R.id.frame,ChatMessageFragment(),"")
            .commit()
    }
    private fun showPrivacyPolicyFragment(){
        supportFragmentManager.beginTransaction()
            .addToBackStack(null)
            .replace(R.id.frame,PrivacyPolicyFragment(),"")
            .commit()
    }
    private fun showTermsAndConditionsFragment(){
        supportFragmentManager.beginTransaction()
            .addToBackStack(null)
            .replace(R.id.frame,TermsAndConditionsFragment(),"")
            .commit()
    }

    private fun showMyLisitngsFragment(){
        supportFragmentManager.beginTransaction()
            .addToBackStack(null)
            .replace(R.id.frame,MyListingsFragment(),"")
            .commit()
    }



    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_option_menu,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            R.id.menu_logout->confirmLogOut()
            R.id.menu_messages-> goToOtherViewActivity(this,Constants.CHAT_MESSAGES)
            R.id.menu_notifications-> goToOtherViewActivity(this,Constants.NOTIFICATIONS_LIST)
            R.id.sub_menu_about-> goToOtherViewActivity(this,Constants.ABOUT_US)
            R.id.sub_menu_contact-> goToOtherViewActivity(this,Constants.CONTACT_US)
            R.id.sub_menu_privacy_policy-> goToOtherViewActivity(this,Constants.PRIVACY_POLICY)
            R.id.sub_menu_terms-> goToOtherViewActivity(this,Constants.TERMS_CONDITIONS)
            R.id.menu_my_list-> goToOtherViewActivity(this,Constants.MY_LISTINGS)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun onLogOut() {
        mySharedPreferences.clearDatas()
        goToLoginActivity(this)
    }

    fun confirmLogOut(){
        AlertDialog.Builder(this)
            .setMessage("Are you sure want to LogOut")
            .setCancelable(false)
            .setPositiveButton("Yes"
            ) { dialog, which ->
                onLogOut()
            }
            .setNegativeButton("No",null)
            .show()
    }


    private fun injectDependency() {
        val activityComponent = DaggerActivityComponent.builder()
            .activityModule(ActivityModule(this))
            .sharedPreferenceModule(SharedPreferenceModule())
            .contextModule(ContextModule(this))
            .build()
        activityComponent.inject(this)
    }

    override fun onBackPressed() {
       finish()
        //goToMainActivity(this)
    }
}
