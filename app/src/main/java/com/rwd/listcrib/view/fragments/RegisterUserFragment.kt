package com.rwd.listcrib.view.fragments


import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.carveniche.wisdomleap.di.module.SharedPreferenceModule

import com.rwd.listcrib.R
import com.rwd.listcrib.Util.*
import com.rwd.listcrib.api.ApiInterface
import com.rwd.listcrib.di.component.DaggerFragmentComponent
import com.rwd.listcrib.di.module.ContextModule
import com.rwd.listcrib.di.module.FragmentModule
import com.rwd.listcrib.models.NewUserRegisterModel
import com.rwd.listcrib.view.activity.LoginActivity
import com.rwd.listcrib.view.activity.MainActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_add_property.*
import kotlinx.android.synthetic.main.fragment_register_user.*
import kotlinx.android.synthetic.main.layout_progressbar.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import pub.devrel.easypermissions.EasyPermissions
import java.io.File
import java.lang.Exception
import javax.inject.Inject


class RegisterUserFragment : Fragment() {

    private lateinit var rootView: View
    private lateinit var loginActivity: LoginActivity
    @Inject
    lateinit var mySharedPreferences: MySharedPreferences
    private var disposable = CompositeDisposable()
    private var api = ApiInterface.create()
    private var mUserName = ""
    private var mEmail = ""
    private var mState = ""
    private var mCity = ""
    private var mMobileNumber = ""
    private var mPassword = ""
    private val RC_CAMERA_PERM = 123
    private val RC_STORAGE_PERM = 124
    private var photos = mutableListOf<File>()



    fun showProgres(status : Boolean){
        showLoadingProgress(progressBar,status)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_register_user, container, false)
        return rootView
    }

    private fun injectDependency() {
        val fragmentComponent = DaggerFragmentComponent.builder()
            .fragmentModule(FragmentModule())
            .sharedPreferenceModule(SharedPreferenceModule())
            .contextModule(ContextModule(context!!))
            .build()
        fragmentComponent.inject(this)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginActivity = activity as LoginActivity
        injectDependency()
    }
    private fun takePhoto() {
//        EasyImage.configuration(context!!)
//            .setImagesFolderName("ListCrib")
//            .setCopyTakenPhotosToPublicGalleryAppFolder(true)
//            .setCopyPickedImagesToPublicGalleryAppFolder(true)
//            .setAllowMultiplePickInGallery(true);
        EasyImage.openChooserWithGallery(activity!!,"Select Photo",0)
    }
    private fun cameraTask(){
        if(hasCameraPermission(context!!) && hasWriteStoragePermission(context!!)){
            takePhoto()
        }
        else
        {
            if(!hasCameraPermission(context!!))
            {
                requestCameraPermission()
            }
            if(!hasWriteStoragePermission(context!!)){
                requestStoragePermission()
            }

        }
    }
    fun requestCameraPermission(){
        EasyPermissions.requestPermissions(
            this,
            getString(R.string.rationale_camera),
            RC_CAMERA_PERM,
            Manifest.permission.CAMERA)
    }
    fun requestStoragePermission(){
        EasyPermissions.requestPermissions(
            this,
            getString(R.string.rationale_camera),
            RC_CAMERA_PERM,
            Manifest.permission.WRITE_EXTERNAL_STORAGE)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
    }

    private fun initUI() {
        tv_login.setOnClickListener {
            loginActivity.showLoginFragment()
        }
        tv_submit_register.setOnClickListener {
            if(isValidationSuccess())
            {
                submitRegisterDatas()
            }
        }
        iv_camera.setOnClickListener {
            cameraTask()
        }
    }

    private fun isValidationSuccess(): Boolean {
        mUserName = ed_user_name.text.toString()
        mEmail = ed_email_id.text.toString()
        mMobileNumber = ed_phone.text.toString()
        mState = ed_state.text.toString()
        mCity = ed_city.text.toString()
        mPassword = ed_password.text.toString()


        var confirmPassword = ed_confirm_password.text.toString()

        if(mUserName.isEmpty() || mUserName.length<2)
        {
            showShortToast("Name should be minimum 3 characters",context!!)
            return false
        }
        if(!isValidEmail(mEmail))
        {
            showShortToast("Please enter a valid email",context!!)
            return false
        }
//        if(!isValidPhoneNumber(mMobileNumber)){
//            showShortToast("Please enter a valid phone number",context!!)
//            return false
//        }
        if(mCity.isEmpty())
        {
            showShortToast("Please enter a valid city",context!!)
            return false
        }
//        if(mState.isEmpty())
//        {
//            showShortToast("Please enter a valid city",context!!)
//            return false
//        }
        if(mPassword.isEmpty() || confirmPassword.isEmpty())
        {
            showShortToast("Please enter passwords",context!!)
            return false
        }
        if(mPassword != confirmPassword)
        {
            showShortToast("Password and confirm password should be equal",context!!)
            return false
        }

        return true
    }

    fun submitRegisterDatas(){
      //  mCity = "Test City"
        mState = "Test State"
            showProgres(true)
        var requestUserName = RequestBody.create(MediaType.parse("text/plain"),mUserName)
        var requestPassword = RequestBody.create(MediaType.parse("text/plain"),mPassword)
        var requestEmail = RequestBody.create(MediaType.parse("text/plain"),mEmail)
        var requestContact = RequestBody.create(MediaType.parse("text/plain"),mMobileNumber)
        var requestAddress = RequestBody.create(MediaType.parse("text/plain"),"Test Address")
        var requestCity = RequestBody.create(MediaType.parse("text/plain"),mCity)
        var requestState = RequestBody.create(MediaType.parse("text/plain"),mState)

        if(photos.isEmpty())
        {
            var regObservable = api.addNewUser(requestUserName,requestPassword,requestEmail,requestContact,requestAddress,requestCity,requestState)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgres(false)
                    if(it.errors.isNullOrEmpty())
                    {
                        storeUserData(it)
                    }
                    else{
                        toastErrorMessage(context!!,it.errors)
                    }


                },{
                    showProgres(false)
                    Log.d(Constants.LOG_TAG,it.localizedMessage)
                })

            disposable.add(regObservable)
        }
        else
        {
            var file = File(photos[0].path)
            var surveyBody = RequestBody.create(MediaType.parse("image/*"), file)
            var requestImage = MultipartBody.Part.createFormData("f_image",file.name,surveyBody)
            var regObservable = api.addNewUserWithImage(requestUserName,requestPassword,requestEmail,requestContact,requestAddress,requestCity,requestState,requestImage)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgres(false)
                    if(it.errors.isNullOrEmpty())
                    {
                        storeUserData(it)
                    }
                    else{
                        toastErrorMessage(context!!,it.errors)
                    }


                },{
                    showProgres(false)
                    Log.d(Constants.LOG_TAG,it.localizedMessage)
                })

            disposable.add(regObservable)
        }




    }



    fun storeUserData(newUserRegisterModel : NewUserRegisterModel){
        try {
            mySharedPreferences.putBoolean(Constants.IS_LOGGED,true)
            mySharedPreferences.putString(Constants.USER_NAME,newUserRegisterModel.data.name)
           // mySharedPreferences.putString(Constants.USER_CONTACT,newUserRegisterModel.data.contact)
            mySharedPreferences.putString(Constants.USER_EMAIL,newUserRegisterModel.data.email)
            mySharedPreferences.putString(Constants.USER_TOKEN,newUserRegisterModel.token)
            mySharedPreferences.putIntData(Constants.USER_ID,newUserRegisterModel.data.id)
            goToMainActivity()
        }
        catch (ae : Exception){
            Log.d(Constants.LOG_TAG,"Register Exception err : "+ae.localizedMessage)
        }


    }

    private fun goToMainActivity() {
        var intent = Intent(activity,MainActivity::class.java)
        startActivity(intent)
    }

    companion object {
        const val TAG = "RegisterUserFragment"
    }
    fun onPhotosReturned(returnedPhotos : List<File>){
        photos.addAll(returnedPhotos)

        displayAddedImages()
    }
    private fun displayAddedImages() {
        Log.d(Constants.LOG_TAG,photos[0].path)
        var imgBitmap = getBitmapFromPath(photos[0].path)
        iv_avatar.setImageBitmap(imgBitmap)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        EasyImage.handleActivityResult(requestCode, resultCode, data, activity!!, object : DefaultCallback() {
            override fun onImagesPicked(p0: MutableList<File>, p1: EasyImage.ImageSource?, p2: Int) {
                Log.d(Constants.LOG_TAG,"p0 : $p0 p1: $p1 p2 : $p2")

                onPhotosReturned(p0)

            }

            override fun onCanceled(source: EasyImage.ImageSource?, type: Int) {
                super.onCanceled(source, type)
            }

            override fun onImagePickerError(e: Exception?, source: EasyImage.ImageSource?, type: Int) {
                super.onImagePickerError(e, source, type)
                Log.e(Constants.LOG_TAG,e.toString())
            }

        })
    }


}
