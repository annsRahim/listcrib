package com.rwd.listcrib.view.fragments


import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.carveniche.wisdomleap.di.module.SharedPreferenceModule

import com.rwd.listcrib.R
import com.rwd.listcrib.Util.Constants
import com.rwd.listcrib.Util.MySharedPreferences
import com.rwd.listcrib.Util.showLoadingProgress
import com.rwd.listcrib.Util.showShortToast
import com.rwd.listcrib.adapter.PropertyListAdapter
import com.rwd.listcrib.api.ApiInterface
import com.rwd.listcrib.contract.MainListContract
import com.rwd.listcrib.di.component.DaggerFragmentComponent
import com.rwd.listcrib.di.module.ContextModule
import com.rwd.listcrib.di.module.FragmentModule
import com.rwd.listcrib.interfaces.IFavouriteSelectedListener
import com.rwd.listcrib.models.NewPropertyListModel
import com.rwd.listcrib.models.propertListModel.AllPropertyListModel
import com.rwd.listcrib.presenter.MainListPresenter
import com.rwd.listcrib.view.activity.AddPropertyActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_main_list.*
import kotlinx.android.synthetic.main.layout_progressbar.*
import javax.inject.Inject


class MainListFragment : Fragment(),MainListContract.View,IFavouriteSelectedListener {



    @Inject lateinit var presenter : MainListContract.Presenter
    @Inject lateinit var mySharedPreferences: MySharedPreferences
    private lateinit var rootView : View
    private lateinit var newPropertyList  : AllPropertyListModel
    private var api = ApiInterface.create()
    private var disposable = CompositeDisposable()



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_main_list, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependency()
        api = ApiInterface.create(context!!)
    }
    private fun injectDependency() {
        val fragmentComponent = DaggerFragmentComponent.builder()
            .fragmentModule(FragmentModule())
            .sharedPreferenceModule(SharedPreferenceModule())
            .contextModule(ContextModule(context!!))
            .build()
        fragmentComponent.inject(this)

    }

    override fun showProgress(show: Boolean) {
        showLoadingProgress(progressBar,show)
    }

    private fun initUI() {
        presenter.attach(this)
        presenter.subscribe()
        loadPropertyList()

        tv_add_property.setOnClickListener {
            goToAddPropertyActivity()
        }

    }

    private fun loadPropertyList() {
        var listDisposable = api.listAllProperty()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Log.d(Constants.LOG_TAG,it.toString())
                onListLoadDataSuccess(it)
            },{
                Log.d(Constants.LOG_TAG,it.localizedMessage)
                onListLoadDataFailed(it.localizedMessage)
            })
        disposable.add(listDisposable)
    }

    private fun goToAddPropertyActivity() {
        var intent = Intent(context!!,AddPropertyActivity::class.java)
        startActivity(intent)
    }

    private fun displayDatas() {
        rv_new_list.layoutManager = LinearLayoutManager(context!!);
        rv_new_list.adapter = PropertyListAdapter(context!!,newPropertyList.data,this,activity!!)
    }

    override fun onListLoadDataSuccess(propertyListModel: AllPropertyListModel) {
        this.newPropertyList = propertyListModel
        Log.d(Constants.LOG_TAG,newPropertyList.toString())
        displayDatas()
    }

    override fun onListLoadDataFailed(msg: String) {
        showShortToast("Error : $msg",context!!)
    }
    companion object {
        const val TAG = "MainListFragment"
    }

    fun addFavouriteProperty(propertyID: Int){
        var addObservable = api.addLidtFav(propertyID)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({},{
                Log.d(Constants.LOG_TAG,"Add Fav list failed : ${it.localizedMessage}")
            })
        disposable.add(addObservable)
    }
    fun removeFavouriteProperty(propertyID: Int){
        var removeObseravble = api.removeLidtFav(propertyID)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({},{
                Log.d(Constants.LOG_TAG,"Remove Fav list failed : ${it.localizedMessage}")
            })
        disposable.add(removeObseravble)
    }

    override fun onFavouriteSelected(status: Boolean, propertyID: Int) {
        var regrexIDCHECK = "+${propertyID}+"
        var favouriteList = mySharedPreferences.getString(Constants.FAVOURITE_LIST)
        if(status)
        {
            favouriteList += regrexIDCHECK
                addFavouriteProperty(propertyID)
            Log.d(Constants.LOG_TAG,"Fav Selcted $propertyID")
        }
        else{
            favouriteList = favouriteList.replace(regrexIDCHECK,"")
            Log.d(Constants.LOG_TAG,"This is favuor $favouriteList")
            removeFavouriteProperty(propertyID)
            Log.d(Constants.LOG_TAG,"Fav Removed $propertyID")
        }
        mySharedPreferences.putString(Constants.FAVOURITE_LIST,favouriteList)

    }


}
