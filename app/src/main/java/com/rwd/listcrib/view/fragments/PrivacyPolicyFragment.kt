package com.rwd.listcrib.view.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.rwd.listcrib.R
import kotlinx.android.synthetic.main.fragment_privacy_policy.*

/**
 * A simple [Fragment] subclass.
 */
class PrivacyPolicyFragment : Fragment() {

    private var contentText = ""


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_privacy_policy, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
    }

    private fun initUI() {
        contentText = "At ListCrib.com, accessible from listcrib.com, one of our main priorities is the privacy of our visitors. This Privacy Policy document contains types of information that is collected and recorded by ListCrib.com and how we use it.\n" +
                "\n" +
                "If you have additional questions or require more information about our Privacy Policy, do not hesitate to contact us.\n" +
                "\n" +
                "Log Files\n" +
                "ListCrib.com follows a standard procedure of using log files. These files log visitors when they visit sites. All hosting companies do this and a part of hosting services' analytics. The information collected by log files include internet protocol (IP) addresses, browser type, Internet Service Provider (ISP), date and time stamp, referring/exit pages, and possibly the number of clicks. These are not linked to any information that is personally identifiable. The purpose of the information is for analyzing trends, administering the site, tracking users' movement on the site, and gathering demographic information.\n" +
                "\n" +
                "Cookies and Web Beacons\n" +
                "Like any other site, ListCrib.com uses 'cookies'. These cookies are used to store information including visitors' preferences, and the pages on the site that the visitor accessed or visited. The information is used to optimize the users' experience by customizing our page content based on visitors' browser type and/or other information.\n" +
                "\n" +
                "For more general information on cookies, please read \"What Are Cookies\".\n" +
                "\n" +
                "Our Advertising Partners\n" +
                "Some of advertisers on our site may use cookies and beacons. Our advertising partners are listed below. Each of our advertising partners has their own Privacy Policy for their policies on user data. For easier access, we hyperlinked to their Privacy Policies below\n" +
                "\n" +
                "Privacy Policies\n" +
                "You may consult this list to find the Privacy Policy for each of the advertising partners of ListCrib.com. Our Privacy Policy was created with the help of the Free Privacy Policy Generator and the Privacy Policy Generator Online.\n" +
                "\n" +
                "Third-party ad servers or ad networks uses technologies like cookies, JavaScript, or Site Beacons that are used in their respective advertisements and links that appear on ListCrib.com, which are sent directly to users' browser. They automatically receive your IP address when this occurs. These technologies are used to measure the effectiveness of their advertising campaigns and/or to personalize the advertising content that you see on sites that you visit.\n" +
                "\n" +
                "Note that ListCrib.com has no access to or control over these cookies that are used by third-party advertisers.\n" +
                "\n" +
                "Third Party Privacy Policies\n" +
                "ListCrib.com's Privacy Policy does not apply to other advertisers or sites. Thus, we are advising you to consult the respective Privacy Policies of these third-party ad servers for more detailed information. It may include their practices and instructions about how to opt-out of certain options. You may find a complete list of these Privacy Policies and their links here: Privacy Policy Links.\n" +
                "\n" +
                "You can choose to disable cookies through your individual browser options. To know more detailed information about cookie management with specific site browsers, it can be found at the browsers' respective sites. What Are Cookies?\n" +
                "\n" +
                "Children's Information\n" +
                "Another part of our priority is adding protection for children while using the internet. We encourage parents and guardians to observe, participate in, and/or monitor and guide their online activity.\n" +
                "\n" +
                "ListCrib.com does not knowingly collect any Personal Identifiable Information from children under the age of 13. If you think that your child provided this kind of information on our site, we strongly encourage you to contact us immediately and we will do our best efforts to promptly remove such information from our records.\n" +
                "\n" +
                "Online Privacy Policy Only\n" +
                "This Privacy Policy applies only to our online activities and is valid for visitors to our website with regards to the information that they shared and/or collect in ListCrib.com. This policy is not applicable to any information collected offline or via channels other than this site.\n" +
                "\n" +
                "Consent\n" +
                "By using our site, you hereby consent to our Privacy Policy and agree to its Terms and Conditions."

            tv_content.text = contentText
    }

}
