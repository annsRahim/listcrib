package com.rwd.listcrib.view.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import com.carveniche.wisdomleap.di.module.SharedPreferenceModule
import com.rwd.listcrib.R
import com.rwd.listcrib.Util.Constants
import com.rwd.listcrib.Util.MySharedPreferences
import com.rwd.listcrib.Util.hashFromSHA1
import com.rwd.listcrib.di.component.DaggerActivityComponent
import com.rwd.listcrib.di.module.ActivityModule
import com.rwd.listcrib.di.module.ContextModule
import javax.inject.Inject

class SplashActivity : AppCompatActivity() {

    @Inject lateinit var mySharedPreferences: MySharedPreferences

    private var mDelayHandler: Handler? = null
    private val SPLASH_DELAY: Long = 3000 //3 seconds

    internal val mRunnable: Runnable = Runnable {
        if (!isFinishing) {
            checkIsLoggedIn()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        injectDependency()
        try{
            hashFromSHA1("SHA1: B0:B0:43:DB:54:AF:72:1C:F6:D9:7B:56:F6:51:66:DE:AB:0E:27:43")
        }
        catch (ae : Exception){
            Log.e(Constants.LOG_TAG,ae.localizedMessage)
        }
        //Initialize the Handler
        mDelayHandler = Handler()

        //Navigate with delay
        mDelayHandler!!.postDelayed(mRunnable, SPLASH_DELAY)


    }

    private fun checkIsLoggedIn() {
        var isLogged = mySharedPreferences.getBoolean(Constants.IS_LOGGED,false)
        Log.d(Constants.LOG_TAG," LOGGED $isLogged")
        if(mySharedPreferences.getBoolean(Constants.IS_LOGGED,false))
        {
            goToMainActivity()
        }
        else
        {
            goToLoginActivity()
        }
    }

    private fun goToMainActivity() {
        var intent  = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    private fun goToLoginActivity(){
        var intent  = Intent(this,LoginActivity::class.java)
        startActivity(intent)
    }

    private fun injectDependency() {
        val activityComponent = DaggerActivityComponent.builder()
            .activityModule(ActivityModule(this))
            .sharedPreferenceModule(SharedPreferenceModule())
            .contextModule(ContextModule(this))
            .build()
        activityComponent.inject(this)
    }
    public override fun onDestroy() {

        if (mDelayHandler != null) {
            mDelayHandler!!.removeCallbacks(mRunnable)
        }

        super.onDestroy()
    }
}
