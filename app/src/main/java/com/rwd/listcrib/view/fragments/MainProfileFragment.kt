package com.rwd.listcrib.view.fragments


import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.rwd.listcrib.R
import com.rwd.listcrib.Util.Constants
import com.rwd.listcrib.Util.getBitmapFromPath
import com.rwd.listcrib.Util.hasCameraPermission
import com.rwd.listcrib.Util.hasWriteStoragePermission
import com.rwd.listcrib.api.ApiInterface
import com.rwd.listcrib.models.UserProfileModel
import com.rwd.listcrib.view.activity.EditProfileActivity
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_main_profile.*
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import pub.devrel.easypermissions.EasyPermissions
import java.io.File
import java.lang.Exception


class MainProfileFragment : Fragment() {
    private var api = ApiInterface.create()
    private var disposable = CompositeDisposable()
    private lateinit var userProfileModel: UserProfileModel
    private var photos = mutableListOf<File>()
    private val RC_CAMERA_PERM = 123
    private val RC_STORAGE_PERM = 124

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_profile, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        api = ApiInterface.create(context!!)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
        getProfileDatas()
    }

    private fun initUI() {
        iv_camera.setOnClickListener {
            cameraTask()
        }
        tv_edit_profile.setOnClickListener {
            openEditProfileActivity()
        }

    }

    fun openEditProfileActivity(){
        var intent = Intent(context!!,EditProfileActivity::class.java)
        intent.putExtra(Constants.USER_NAME,userProfileModel.name)
        intent.putExtra(Constants.USER_EMAIL,userProfileModel.email)
        intent.putExtra(Constants.USER_CONTACT,userProfileModel.contact)
        intent.putExtra(Constants.USER_ADDRESS,userProfileModel.user_address)
        intent.putExtra(Constants.USER_IMAGE,userProfileModel.image)
        startActivity(intent)
    }

    private fun cameraTask(){
        if(hasCameraPermission(context!!) && hasWriteStoragePermission(context!!)){
            takePhoto()
        }
        else
        {
            if(!hasCameraPermission(context!!))
            {
                requestCameraPermission()
            }
            if(!hasWriteStoragePermission(context!!)){
                requestStoragePermission()
            }

        }
    }

    private fun takePhoto() {
        EasyImage.configuration(context!!)
            .setImagesFolderName("ListCrib")
            .setCopyTakenPhotosToPublicGalleryAppFolder(true)
            .setCopyPickedImagesToPublicGalleryAppFolder(true)
            .setAllowMultiplePickInGallery(true);
        EasyImage.openChooserWithGallery(this,"Choose Image",0)
    }
    fun requestCameraPermission(){
        EasyPermissions.requestPermissions(
            this,
            getString(R.string.rationale_camera),
            RC_CAMERA_PERM,
            Manifest.permission.CAMERA)
    }
    fun requestStoragePermission(){
        EasyPermissions.requestPermissions(
            this,
            getString(R.string.rationale_camera),
            RC_CAMERA_PERM,
            Manifest.permission.WRITE_EXTERNAL_STORAGE)
    }

    private fun getProfileDatas() {
        var profileDisposable = api.getUserProfile()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                    if(it.errors.isNullOrEmpty())
                    {
                        this.userProfileModel = it
                        displayProfileDatas()
                    }


            },{
                Log.d(Constants.LOG_TAG,"Unable to load Profile : ${it.localizedMessage}")
            })
        disposable.add(profileDisposable)
    }

    private fun displayProfileDatas() {
        tv_user_name.text = userProfileModel.name
        tv_email.text = "Email - "+userProfileModel.email
        if(userProfileModel.contact.isNullOrEmpty())
            tv_phone.text = "Phone - "
        else
            tv_phone.text = "Phone - "+userProfileModel.contact
        if(userProfileModel.user_address.isNullOrEmpty())
            tv_address.text = "Address - "
        else
            tv_address.text = "Address - "+userProfileModel.user_address
        if(!userProfileModel.image.isNullOrEmpty())
        {
            Picasso.with(context).load(userProfileModel.image)
                .into(iv_avatar)
        }
    }

    fun onPhotosReturned(returnedPhotos : List<File>){
        photos.addAll(returnedPhotos)

        displayAddedImages()
    }

    private fun displayAddedImages() {
        Log.d(Constants.LOG_TAG,photos[0].path)
        var imgBitmap = getBitmapFromPath(photos[0].path)
        iv_avatar.setImageBitmap(imgBitmap)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        EasyImage.handleActivityResult(requestCode, resultCode, data, activity, object : DefaultCallback() {
            override fun onImagesPicked(p0: MutableList<File>, p1: EasyImage.ImageSource?, p2: Int) {
                Log.d(Constants.LOG_TAG,"p0 : $p0 p1: $p1 p2 : $p2")

                    onPhotosReturned(p0)

            }

            override fun onCanceled(source: EasyImage.ImageSource?, type: Int) {
                super.onCanceled(source, type)
            }

            override fun onImagePickerError(e: Exception?, source: EasyImage.ImageSource?, type: Int) {
                super.onImagePickerError(e, source, type)
                Log.e(Constants.LOG_TAG,e.toString())
            }

        })
    }

    companion object {
        const val TAG = "MainProfileFragment"
    }

}
