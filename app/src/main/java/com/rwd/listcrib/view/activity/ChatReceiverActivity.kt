package com.rwd.listcrib.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.ImageView
import androidx.appcompat.app.ActionBar
import androidx.recyclerview.widget.LinearLayoutManager
import com.carveniche.wisdomleap.di.module.SharedPreferenceModule
import com.rwd.listcrib.R
import com.rwd.listcrib.Util.Constants
import com.rwd.listcrib.Util.MySharedPreferences
import com.rwd.listcrib.Util.showLoadingProgress
import com.rwd.listcrib.Util.showShortToast
import com.rwd.listcrib.adapter.MessageListAdapter
import com.rwd.listcrib.api.ApiInterface
import com.rwd.listcrib.di.component.DaggerActivityComponent
import com.rwd.listcrib.di.module.ActivityModule
import com.rwd.listcrib.di.module.ContextModule
import com.rwd.listcrib.models.ChatReceiverTokenModel
import com.rwd.listcrib.models.ChatTokenModel
import com.twilio.chat.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_add_property.*
import kotlinx.android.synthetic.main.activity_chat_user.*
import kotlinx.android.synthetic.main.layout_progressbar.*
import javax.inject.Inject


class ChatReceiverActivity : AppCompatActivity(),ChannelListener{

    @Inject
    lateinit var mySharedPreferences: MySharedPreferences
    private lateinit var mChatClient: ChatClient
    private lateinit var mChannel : Channel
    private var api = ApiInterface.create()
    private var disposable = CompositeDisposable()
    private lateinit var chatTokenModel: ChatReceiverTokenModel
    private var mChannelSid = ""
    private  var messageList = mutableListOf<Message>()
    private lateinit var messageListAdapter: MessageListAdapter
    private var mCurrentUserID = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_user)
        injectDependency()
        mCurrentUserID = mySharedPreferences.getIntData(Constants.USER_ID)
        api = ApiInterface.create(this)
        mChannelSid = intent.getStringExtra(Constants.CHANNEL_SID)
       // mChannelSid = "CH586454abf41d40ba80fd24c1e1c6b87f"
        try{
            initUI()
            loadChatToken()
        }
        catch (ae : Exception){
           Log.e(Constants.LOG_TAG,"Chat Exception ${ae.localizedMessage}")
        }

    }

    private fun showProgress(status : Boolean)
    {
        showLoadingProgress(progressBar,status)
    }

    private fun loadChatToken() {
        showProgress(true)
        var chatDisposable = api.chatReceiverToken()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

                    //showProgress(false)
                    this.chatTokenModel = it
                   initChatClient()


            },{
                showProgress(false)
                showShortToast(it.localizedMessage,this)
                Log.d(Constants.LOG_TAG,it.localizedMessage)
            })
        disposable.add(chatDisposable)
    }

    private fun injectDependency() {
        val activityComponent = DaggerActivityComponent.builder()
            .activityModule(ActivityModule(this))
            .sharedPreferenceModule(SharedPreferenceModule())
            .contextModule(ContextModule(this))
            .build()
        activityComponent.inject(this)
    }



    private fun customActionBar() {
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        supportActionBar!!.setDisplayShowCustomEnabled(true);
        supportActionBar!!.setCustomView(R.layout.action_bar_view);
        var view = supportActionBar!!.customView
        var ivActionBack = view.findViewById<ImageView>(R.id.iv_action_back)
        ivActionBack.setOnClickListener {
            onBackPressed()
        }
    }
    private fun initUI() {
        customActionBar()
        messageListAdapter = MessageListAdapter(this,mCurrentUserID.toString(),messageList)
        rv_messages.layoutManager = LinearLayoutManager(this);
        rv_messages.adapter = messageListAdapter
        iv_send_message.setOnClickListener {
            sendMessageToChannel()
        }

    }

    private fun initChatClient() {
        var props = ChatClient.Properties.Builder().setRegion("us1").createProperties()
        Log.d(Constants.LOG_TAG,"Intiial")
        ChatClient.create(this,chatTokenModel.token,props,object : CallbackListener<ChatClient>(){
            override fun onSuccess(p0: ChatClient?) {
                Log.d(Constants.LOG_TAG,"Client Initialized")
                mChatClient = p0!!
                joinAChannel()
            }

            override fun onError(errorInfo: ErrorInfo?) {
                showProgress(false)
                Log.d(Constants.LOG_TAG,"Chat Initialized Error : $errorInfo")
            }

        })
    }

    private fun joinAChannel(){

        Handler().postDelayed({
            mChatClient.channels.getChannel(mChannelSid,object : CallbackListener<Channel>(){
                override fun onSuccess(p0: Channel?) {
                    Log.d(Constants.LOG_TAG,"Success on Channel $p0")
                    mChannel = p0!!
                    Handler().postDelayed({
                        getMessagesFromChannel()
                    },2000)

                }

                override fun onError(errorInfo: ErrorInfo?) {
                    showProgress(false)
                    super.onError(errorInfo)
                    Log.d(Constants.LOG_TAG,"Error on Channel $errorInfo")
                }

            })
        }, 0)




    }

    private fun getMessagesFromChannel(){
        try{
            mChannel.messages.getLastMessages(50,object : CallbackListener<List<Message>>(){
                override fun onSuccess(p0: List<Message>?) {
                    Log.d(Constants.LOG_TAG,"Success on getting message "+p0!!.size)
                    messageList = p0.toMutableList()
                    showProgress(false)
                    displayMessageDatas()
                }

                override fun onError(errorInfo: ErrorInfo?) {
                    showProgress(false)
                    super.onError(errorInfo)
                    Log.d(Constants.LOG_TAG,"Error on getting message $errorInfo")
                }

            })
        }
        catch (ae : Exception){
            Log.e(Constants.LOG_TAG,"Exception ${ae.localizedMessage}")
        }

    }

    private fun displayMessageDatas(){
        messageListAdapter = MessageListAdapter(this,mCurrentUserID.toString(),messageList)
        var llManager = LinearLayoutManager(this);

        rv_messages.layoutManager = llManager
        rv_messages.adapter = messageListAdapter
        rv_messages.scrollToPosition(messageList.size - 1)
    }
    private fun refreshMessageList(){
        displayMessageDatas()
       // messageListAdapter.notifyDataSetChanged()
    }

    private fun sendMessageToChannel(){
        if(ed_message_text.text.toString().isNullOrEmpty())
        {
            showShortToast("Enter a text to send",this)
            return
        }
        try{
            var messageText = ed_message_text.text.toString()
            mChannel.addListener(this)

            Log.d(Constants.LOG_TAG,mChannel.status.toString())
            mChannel.messages.sendMessage(Message.options().withBody(messageText),object : CallbackListener<Message>(){
                override fun onSuccess(p0: Message?) {
                    ed_message_text.text.clear()

                    Log.d(Constants.LOG_TAG,"Message : $p0")
                }

                override fun onError(errorInfo: ErrorInfo?) {
                    super.onError(errorInfo)
                    Log.d(Constants.LOG_TAG,"Messsage Sending Failed : $errorInfo")
                }

            })
        }
        catch (ae : Exception){
            Log.d(Constants.LOG_TAG,"Exception in send message ${ae.localizedMessage}")
        }

    }

    override fun onMemberDeleted(p0: Member?) {

    }

    override fun onTypingEnded(p0: Channel?, p1: Member?) {
        Log.d(Constants.LOG_TAG,"Typing Ended "+p1)
    }

    override fun onMessageAdded(p0: Message?) {
        Log.d(Constants.LOG_TAG,"Message Added Listener "+p0!!.author+" -- "+mChannelSid)
        messageList.add(p0!!)
        refreshMessageList()
    }

    override fun onMessageDeleted(p0: Message?) {

    }

    override fun onMemberAdded(p0: Member?) {

    }

    override fun onTypingStarted(p0: Channel?, p1: Member?) {
        Log.d(Constants.LOG_TAG,"Typing Started "+p1)
    }

    override fun onSynchronizationChanged(p0: Channel?) {

    }

    override fun onMessageUpdated(p0: Message?, p1: Message.UpdateReason?) {

    }

    override fun onMemberUpdated(p0: Member?, p1: Member.UpdateReason?) {

    }


}
