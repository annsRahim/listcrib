package com.rwd.listcrib.view.fragments

import android.app.Dialog
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.provider.SyncStateContract
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.extractor.ExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelection
import com.google.android.exoplayer2.trackselection.TrackSelector
import com.google.android.exoplayer2.upstream.BandwidthMeter
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.rwd.listcrib.R
import com.rwd.listcrib.Util.Constants
import com.rwd.listcrib.adapter.ImageGalleryAdapter
import kotlinx.android.synthetic.main.dialog_image_gallery.*
import kotlinx.android.synthetic.main.dialog_video_play.view.*
import org.json.JSONArray
import java.lang.Exception
import java.net.URLDecoder


class ImageGalleryDialogFragment(var mContext : Context,var listImage : String) : DialogFragment() {
    private lateinit var rootView : View


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogStyle);
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.dialog_image_gallery,container,false)
        return rootView
    }

    override fun onStart() {
        super.onStart()

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        try{
            initImageGallery()
        }
        catch (ae : Exception){
          Log.e(Constants.LOG_TAG,ae.localizedMessage)
        }

    }

    private fun initImageGallery() {
        var jsonArray = JSONArray()
        listImage = URLDecoder.decode(listImage, "UTF-8");
        Log.d(Constants.LOG_TAG,"String images $listImage")
        try {
            jsonArray = JSONArray(listImage)
        }
        catch (ae : Exception){
         jsonArray.put(0,listImage)
        }

        Log.d(Constants.LOG_TAG,"Json images $jsonArray")
        var adapter = ImageGalleryAdapter(mContext,jsonArray)
        viewPager.adapter = adapter
    }


    override fun onPause() {
        super.onPause()

    }

    override fun onDestroy() {
        super.onDestroy()

    }


}