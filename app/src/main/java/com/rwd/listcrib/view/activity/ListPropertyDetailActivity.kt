package com.rwd.listcrib.view.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import com.carveniche.wisdomleap.di.module.SharedPreferenceModule
import com.rwd.listcrib.R
import com.rwd.listcrib.Util.*
import com.rwd.listcrib.api.ApiInterface
import com.rwd.listcrib.di.component.DaggerActivityComponent
import com.rwd.listcrib.di.module.ActivityModule
import com.rwd.listcrib.di.module.ContextModule
import com.rwd.listcrib.models.DetailData
import com.rwd.listcrib.view.fragments.ImageGalleryDialogFragment
import com.rwd.listcrib.view.fragments.VideoPlayDialogFragment
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_list_property_detail.*
import kotlinx.android.synthetic.main.layout_progressbar.*
import javax.inject.Inject

class ListPropertyDetailActivity : AppCompatActivity() {
    @Inject lateinit var mySharedPreferences: MySharedPreferences
    private var api = ApiInterface.create()
    private var dispossable = CompositeDisposable()
    private var mPropertyId = 0
    private lateinit var detailData: DetailData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_property_detail)

        api = ApiInterface.create(this)
        mPropertyId = intent.getIntExtra(Constants.PROPERTY_ID,0)
        Log.d(Constants.LOG_TAG,"Property $mPropertyId")
        injectDependency()
        initUI()

    }

    private fun customActionBar() {
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        supportActionBar!!.setDisplayShowCustomEnabled(true);
        supportActionBar!!.setCustomView(R.layout.action_bar_view);
        var view = supportActionBar!!.customView
        var ivActionBack = view.findViewById<ImageView>(R.id.iv_action_back)
        ivActionBack.setOnClickListener {
            onBackPressed()
        }
    }




    private fun injectDependency() {
        val activityComponent = DaggerActivityComponent.builder()
            .activityModule(ActivityModule(this))
            .sharedPreferenceModule(SharedPreferenceModule())
            .contextModule(ContextModule(this))
            .build()
        activityComponent.inject(this)
    }

    private fun showProgress(status : Boolean)
    {
        showLoadingProgress(progressBar,status)
    }

    private fun initUI() {
        customActionBar()
        iv_video_play.setOnClickListener {
            showVideoPlayDialog()
        }
        tv_chat_user.setOnClickListener {
            goToChatUserActivity()
        }
        tv_edit_post.setOnClickListener {
            onEditPostClick()
        }
        iv_photo_gallery.setOnClickListener {
            onShowImageGallery()
        }
        tv_add_property.setOnClickListener {
            goToAddPropertyActivity(this)
        }
        iv_add_property.setOnClickListener {
            goToAddPropertyActivity(this)
        }
        loadDetailDatas()
    }

    private fun onShowImageGallery() {
        if(detailData.listing_image.isNullOrEmpty())
        {
            showShortToast("Images not available for the this property",this)
            return
        }

        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val prev = supportFragmentManager.findFragmentByTag("dialog")
        if (prev != null) {
            fragmentTransaction.remove(prev)
        }
        fragmentTransaction.addToBackStack(null)
        val dialogFragment =ImageGalleryDialogFragment(this,detailData.listing_image)//here MyDialog is my custom dialog
        dialogFragment.show(fragmentTransaction, "dialog")

    }

    private fun onEditPostClick() {
        var intent = Intent(this,EditPropertyActivity::class.java)
        intent.putExtra(Constants.PROPERTY_ID,mPropertyId)
        startActivity(intent)
    }

    private fun goToChatUserActivity() {
        var intent = Intent(this,ChatUserActivity::class.java)
        intent.putExtra(Constants.PROPERTY_ID,mPropertyId)
        startActivity(intent)
    }

    private fun showVideoPlayDialog() {
        if(detailData.listing_video.isNullOrEmpty())
        {
            showShortToast("Video not available for the this property",this)
            return
        }

        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val prev = supportFragmentManager.findFragmentByTag("dialog")
        if (prev != null) {
            fragmentTransaction.remove(prev)
        }
        fragmentTransaction.addToBackStack(null)
        val dialogFragment =VideoPlayDialogFragment(this,detailData.listing_video)//here MyDialog is my custom dialog
        dialogFragment.show(fragmentTransaction, "dialog")

    }

    private fun loadDetailDatas() {
        showProgress(true)
        var detailObservable = api.propertyDetail(mPropertyId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress(false)
                Log.d(Constants.LOG_TAG,"Property details")
                if(it.status == 200)
                {
                    this.detailData = it.data
                    displayDatas(it.data)
                }
                else{

                }
                Log.d(Constants.LOG_TAG,it.toString())
            },{
                showProgress(false)
                onErrorReponse()

            })
        dispossable.add(detailObservable)
    }

    fun onErrorReponse(){
        showLongToast(Constants.ERROR_RESPONSE,this)
        onBackPressed()
    }

    private fun displayDatas(data: DetailData) {
       var userID =  mySharedPreferences.getIntData(Constants.USER_ID)

        if(data.user_id == userID)
            tv_edit_post.visibility = View.VISIBLE
        else
            tv_edit_post.visibility = View.GONE

        tv_property_use.text = "Property for ${data.listing_use}"
        tv_property_price.text = "Price ${data.listing_price}"
        tv_property_address.text = "${data.listing_address}"
        tv_sqft.text = "${data.listing_sqft} sqft"
        tv_beds.text = "${data.listing_beds_count} Beds"
        tv_baths.text = "${data.listing_baths_count} Baths"
        tv_parking.text = "${data.listing_parking_spot} Parking"
        tv_description.text = "${data.listing_description}"

        var firstImage = Constants.SUBSTITUTE_IMAGE
        if(!data.listing_image.isNullOrEmpty())
            firstImage = getFirstImageFromListingImage(data.listing_image)
        Picasso.with(this).load(firstImage)
            .into(iv_list_image)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_option_menu,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            R.id.menu_logout->confirmLogOut()
            R.id.menu_messages-> goToOtherViewActivity(this,Constants.CHAT_MESSAGES)
            R.id.menu_notifications-> goToOtherViewActivity(this,Constants.NOTIFICATIONS_LIST)
            R.id.sub_menu_about-> goToOtherViewActivity(this,Constants.ABOUT_US)
            R.id.sub_menu_contact-> goToOtherViewActivity(this,Constants.CONTACT_US)
            R.id.sub_menu_privacy_policy-> goToOtherViewActivity(this,Constants.PRIVACY_POLICY)
            R.id.sub_menu_terms-> goToOtherViewActivity(this,Constants.TERMS_CONDITIONS)
            R.id.menu_my_list-> goToOtherViewActivity(this,Constants.MY_LISTINGS)
            android.R.id.home->onBackPressed()
        }
        return true
    }

    fun confirmLogOut(){
        AlertDialog.Builder(this)
            .setMessage("Are you sure want to LogOut")
            .setCancelable(false)
            .setPositiveButton("Yes"
            ) { dialog, which ->
                onLogOut()
            }
            .setNegativeButton("No",null)
            .show()
    }

    private fun onLogOut() {
        mySharedPreferences.clearDatas()
        goToLoginActivity(this)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}
