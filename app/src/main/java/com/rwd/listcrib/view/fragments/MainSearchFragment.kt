package com.rwd.listcrib.view.fragments


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import com.carveniche.wisdomleap.di.module.SharedPreferenceModule

import com.rwd.listcrib.R
import com.rwd.listcrib.Util.Constants
import com.rwd.listcrib.Util.MySharedPreferences
import com.rwd.listcrib.adapter.PropertyListAdapter
import com.rwd.listcrib.adapter.SearchPropertyListAdapter
import com.rwd.listcrib.api.ApiInterface
import com.rwd.listcrib.contract.MainSearchContract
import com.rwd.listcrib.di.component.DaggerFragmentComponent
import com.rwd.listcrib.di.module.ContextModule
import com.rwd.listcrib.di.module.FragmentModule
import com.rwd.listcrib.interfaces.IFavouriteSelectedListener
import com.rwd.listcrib.models.propertListModel.AllPropertyListModel
import com.rwd.listcrib.models.propertListModel.Data
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_main_search.*
import javax.inject.Inject


class MainSearchFragment : Fragment(),MainSearchContract.View, IFavouriteSelectedListener {

    @Inject lateinit var presenter : MainSearchContract.Presenter
    @Inject lateinit var mySharedPreferences: MySharedPreferences
    private lateinit var rootView: View
    private lateinit var newPropertyList  : List<Data>
    private  var filteredList  = mutableListOf<Data>()
    private var mListType  = ""
    private var mListUse  = ""
    private var api = ApiInterface.create()
    private var disposable = CompositeDisposable()



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_main_search, container, false)
        return rootView
    }
    companion object {
        const val TAG = "MainSearchFragment"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependency()
         api = ApiInterface.create(context!!)
    }

    private fun initUI() {
        setListType()
        setSaleType()
        presenter.attach(this)
        presenter.loadPropertyList()
        tv_search.setOnClickListener {
            displayDatas()
        }
    }

    private fun setListType() {
        var listTypeAdapter = ArrayAdapter<String>(context!!,   android.R.layout.simple_spinner_item, Constants.LISTING_TYPE)
        listTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinner_type.adapter = listTypeAdapter;
    }
    private fun setSaleType() {
        var listTypeAdapter = ArrayAdapter<String>(context!!,   android.R.layout.simple_spinner_item, Constants.LISTING_USE)
        listTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinner_sale.adapter = listTypeAdapter;
    }

    private fun injectDependency() {
        val fragmentComponent = DaggerFragmentComponent.builder()
            .fragmentModule(FragmentModule())
            .sharedPreferenceModule(SharedPreferenceModule())
            .contextModule(ContextModule(context!!))
            .build()
        fragmentComponent.inject(this)

    }
    private fun displayDatas() {
        try{
            mListType = spinner_type.selectedItem.toString()
            mListUse = spinner_sale.selectedItem.toString()
            filteredList.clear()


            if(mListType == Constants.SELECT && mListUse == Constants.SELECT)
            {
                newPropertyList.forEach {
                    filteredList.add(it)
                }
            }
            else if(mListUse != Constants.SELECT && mListType != Constants.SELECT){
                newPropertyList.forEachIndexed { index, data ->
                    if(data.listing_type == mListType && data.listing_use == mListUse)
                    {
                        filteredList.add(data)
                    }
                }

            }
            else if(mListType != Constants.SELECT)
            {
                newPropertyList.forEachIndexed { index, data ->

                    if(data.listing_type == mListType)
                    {
                        filteredList.add(data)

                    }

                }
            }
            else if(mListUse !== Constants.SELECT){
                newPropertyList.forEachIndexed { index, data ->

                    if(data.listing_use != mListUse)
                        filteredList.add(data)
                }
            }
            Log.d(Constants.LOG_TAG,"Search SIZE ${filteredList.size}")

            rv_new_list.layoutManager = LinearLayoutManager(context!!);
            rv_new_list.adapter = PropertyListAdapter(context!!,filteredList,this,activity!!)
        }
        catch(ae : Exception){
            Log.e(Constants.LOG_TAG,"Search Error ${ae.localizedMessage} -- ${filteredList.size}")
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
    }

    override fun onListLoadDataSuccess(propertyListModel: AllPropertyListModel) {
        this.newPropertyList = propertyListModel.data
        this.filteredList = propertyListModel.data.toMutableList()
        Log.d(Constants.LOG_TAG,newPropertyList.toString())
        displayDatas()
    }

    override fun onListLoadDataFailed(msg: String) {
        Log.d(Constants.LOG_TAG,msg)
    }

    override fun showProgress(show: Boolean) {

    }

    override fun onFavouriteSelected(status: Boolean, propertyID: Int) {
        var regrexIDCHECK = "+${propertyID}+"
        var favouriteList = mySharedPreferences.getString(Constants.FAVOURITE_LIST)
        if(status)
        {
            favouriteList += regrexIDCHECK
            addFavouriteProperty(propertyID)
            Log.d(Constants.LOG_TAG,"Fav Selcted $propertyID")
        }
        else{
            favouriteList = favouriteList.replace(regrexIDCHECK,"")
            Log.d(Constants.LOG_TAG,"This is favuor $favouriteList")
            removeFavouriteProperty(propertyID)
            Log.d(Constants.LOG_TAG,"Fav Removed $propertyID")
        }
        mySharedPreferences.putString(Constants.FAVOURITE_LIST,favouriteList)
    }

    fun addFavouriteProperty(propertyID: Int){
        var addObservable = api.addLidtFav(propertyID)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({},{
                Log.d(Constants.LOG_TAG,"Add Fav list failed : ${it.localizedMessage}")
            })
        disposable.add(addObservable)
    }
    fun removeFavouriteProperty(propertyID: Int){
        var removeObseravble = api.removeLidtFav(propertyID)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({},{
                Log.d(Constants.LOG_TAG,"Remove Fav list failed : ${it.localizedMessage}")
            })
        disposable.add(removeObseravble)
    }


}
