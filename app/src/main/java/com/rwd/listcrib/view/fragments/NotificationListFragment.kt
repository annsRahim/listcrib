package com.rwd.listcrib.view.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat

import com.rwd.listcrib.R
import kotlinx.android.synthetic.main.fragment_notification_list.*

/**
 * A simple [Fragment] subclass.
 */
class NotificationListFragment : Fragment() {

    private lateinit var rootView: View


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_notification_list, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
    }

    private fun initUI() {
        displayNotifications()
    }

    private fun displayNotifications() {
        var emptyView = TextView(context!!)
        emptyView.text = "No Notifications Found"
        emptyView.setTextColor(ContextCompat.getColor(context!!,R.color.black))

        lv_notification.emptyView = emptyView

    }

    companion object {
        const val TAG = "NotificationFragment"
    }

}
