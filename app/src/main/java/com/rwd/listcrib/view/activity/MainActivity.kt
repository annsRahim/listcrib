package com.rwd.listcrib.view.activity

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView

import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.carveniche.wisdomleap.di.module.SharedPreferenceModule
import com.facebook.AccessToken
import com.facebook.login.LoginManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.rwd.listcrib.R
import com.rwd.listcrib.Util.*
import com.rwd.listcrib.adapter.MainHomeTabAdapter
import com.rwd.listcrib.api.ApiInterface
import com.rwd.listcrib.contract.MainContract
import com.rwd.listcrib.di.component.DaggerActivityComponent
import com.rwd.listcrib.di.module.ActivityModule
import com.rwd.listcrib.di.module.ContextModule
import com.rwd.listcrib.view.fragments.*
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    @Inject
    lateinit var presenter : MainContract.Presenter
    @Inject
    lateinit var mySharedPreferences: MySharedPreferences
    private lateinit var mainHomeTabAdapter: MainHomeTabAdapter
    private var apiInterface = ApiInterface.create()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        injectDependency()
        apiInterface = ApiInterface.create(this)
        getFcmToken(apiInterface)
       // createNotficationChannel(this)
        initUI()

    }



    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
       menuInflater.inflate(R.menu.main_option_menu,menu)
        return true
    }

    private fun initUI() {
        Log.d(Constants.LOG_TAG,mySharedPreferences.getString(Constants.USER_TOKEN))
        mainHomeTabAdapter = MainHomeTabAdapter(supportFragmentManager)
        mainHomeTabAdapter.addFragment(MainHomeFragment(),"Home")
        mainHomeTabAdapter.addFragment(MainSearchFragment(),"Search")
        mainHomeTabAdapter.addFragment(MainListFragment(),"List")
        mainHomeTabAdapter.addFragment(MainFavouriteFragment(),"Favourite")
        mainHomeTabAdapter.addFragment(MainProfileFragment(),"Profile")
        mainHomeTabAdapter.addFragment(NotificationListFragment(),"Notifications")
        mainHomeTabAdapter.addFragment(ChatMessageFragment(),"Messages")
        mainHomeTabAdapter.addFragment(MyListingsFragment(),"My Listings")
        mainHomeTabAdapter.addFragment(PrivacyPolicyFragment(),"Privacy Policy")
        mainHomeTabAdapter.addFragment(TermsAndConditionsFragment(),"Terms Conditions")
        mainHomeTabAdapter.addFragment(ContactUsFragment(),"Help Centre")
        viewPager.adapter = mainHomeTabAdapter
        viewPager.offscreenPageLimit = 5

        customActionBar()



        navigation.setOnNavigationItemSelectedListener(this)
    }

    private fun customActionBar() {
        supportActionBar!!.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        supportActionBar!!.setDisplayShowCustomEnabled(true);
        supportActionBar!!.setCustomView(R.layout.action_bar_view);
        var view = supportActionBar!!.customView
        var ivActionBack = view.findViewById<ImageView>(R.id.iv_action_back)
        ivActionBack.visibility = View.GONE

    }

    private fun injectDependency() {
        val activityComponent = DaggerActivityComponent.builder()
            .activityModule(ActivityModule(this))
            .sharedPreferenceModule(SharedPreferenceModule())
            .contextModule(ContextModule(this))
            .build()
        activityComponent.inject(this)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            R.id.menu_logout->confirmLogOut()
            R.id.menu_messages-> showMessagesFragment()
            R.id.menu_notifications-> showNotificationFragment()
            R.id.sub_menu_about-> goToOtherViewActivity(this,Constants.ABOUT_US)
            R.id.sub_menu_contact-> showhelpCentreFragment()
            R.id.sub_menu_privacy_policy-> showPrivacyPolicyFragment()
            R.id.sub_menu_terms-> showTermsConditionsFragment()
            R.id.menu_my_list-> showMyListingsFragment()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun onLogOut() {
        mySharedPreferences.clearDatas()
        var isFacebookLoggedIN = AccessToken.getCurrentAccessToken() != null
        if(isFacebookLoggedIN)
            LoginManager.getInstance().logOut()
        goToLoginActivity(this)
    }

    fun confirmLogOut(){
        AlertDialog.Builder(this)
            .setMessage("Are you sure want to LogOut")
            .setCancelable(false)
            .setPositiveButton("Yes"
            ) { dialog, which ->
                onLogOut()
            }
            .setNegativeButton("No",null)
            .show()
    }


    private fun showHomeFragment() {
       viewPager.currentItem = 0
    }

    private fun showProfileFragment() {
        viewPager.currentItem = 4
    }

    private fun showFavouritesFragment() {
        viewPager.currentItem = 3
    }


    private fun showSearchFragment() {
        viewPager.currentItem = 1
    }

    public fun showListFragment() {
        viewPager.currentItem = 2
    }

    private fun showNotificationFragment(){
        viewPager.currentItem = 5
    }

    private fun showMessagesFragment(){
        viewPager.currentItem = 6
    }
    private fun showMyListingsFragment(){
        viewPager.currentItem = 7
    }
    private fun showPrivacyPolicyFragment(){
        viewPager.currentItem = 8
    }
    private fun showTermsConditionsFragment(){
        viewPager.currentItem = 9
    }
    private fun showhelpCentreFragment(){
        viewPager.currentItem = 10
    }



    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.navigation_home -> showHomeFragment()
            R.id.navigation_favourite -> showFavouritesFragment()
            R.id.navigation_search -> showSearchFragment()
            R.id.navigation_list -> goToAddPropertyActivity(this)
            R.id.navigation_profile -> showProfileFragment()


        }
        return true
    }

    override fun onBackPressed() {

    }


}
