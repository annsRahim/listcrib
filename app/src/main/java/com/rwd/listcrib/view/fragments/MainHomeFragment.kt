package com.rwd.listcrib.view.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.carveniche.wisdomleap.di.module.SharedPreferenceModule


import com.rwd.listcrib.adapter.MainPropertyNewListAdapter
import com.rwd.listcrib.contract.MainHomeContract
import com.rwd.listcrib.di.component.DaggerFragmentComponent
import com.rwd.listcrib.di.module.ContextModule
import com.rwd.listcrib.di.module.FragmentModule
import com.rwd.listcrib.models.NewPropertyListModel
import kotlinx.android.synthetic.main.fragment_main_home.*
import javax.inject.Inject

import android.content.Context
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import com.rwd.listcrib.R
import com.rwd.listcrib.Util.*
import com.rwd.listcrib.adapter.PropertyListAdapter
import com.rwd.listcrib.api.ApiInterface
import com.rwd.listcrib.interfaces.IFavouriteSelectedListener
import com.rwd.listcrib.models.propertListModel.AllPropertyListModel
import com.rwd.listcrib.view.activity.MainActivity
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.layout_progressbar.*
import java.util.*


class MainHomeFragment : Fragment(),MainHomeContract.View,IFavouriteSelectedListener {


    @Inject lateinit var presenter : MainHomeContract.Presenter
    @Inject lateinit var mySharedPreferences: MySharedPreferences
    private lateinit var rootView: View
    private lateinit var featuredList : AllPropertyListModel
    private lateinit var mainActivity : MainActivity
    private lateinit var newPropertyList : AllPropertyListModel
    private var api = ApiInterface.create()
    private var disposable = CompositeDisposable()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_main_home, container, false)
        return rootView
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependency()
        mainActivity = activity as MainActivity
        api = ApiInterface.create(context!!)
    }
    private fun injectDependency() {
        val fragmentComponent = DaggerFragmentComponent.builder()
            .fragmentModule(FragmentModule())
            .sharedPreferenceModule(SharedPreferenceModule())
            .contextModule(ContextModule(context!!))
            .build()
        fragmentComponent.inject(this)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
    }

    private fun initUI() {
        presenter.attach(this)
        presenter.subscribe()
        presenter.loadFeatureList()
      //  presenter.loadNewPropertyList()
        tv_view_more.setOnClickListener {
            mainActivity.showListFragment()
        }
    }

    private fun displayFeatureistData() {
        var horizontalLayoutManager = LinearLayoutManager(activity,LinearLayoutManager.HORIZONTAL,false)
        rv_featured_list.layoutManager = horizontalLayoutManager
        var listFeaturedPorpety  = featuredList.data
        Collections.reverse(listFeaturedPorpety)
        rv_featured_list.adapter = MainPropertyNewListAdapter(context!!,listFeaturedPorpety)

//        val vi = context!!.applicationContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
//
//
//        featuredList.data.forEach {
//            val v = vi.inflate(R.layout.item_featured_list_main, null)
//            var dataFeature = it
//            v.setOnClickListener {
//                goToListDetailActivity(context!!,dataFeature.id)
//            }
//            var ivProperty = v.findViewById<ImageView>(R.id.iv_property)
//            val tvPropertyName  = v.findViewById<TextView>(R.id.tv_property_name)
//            val tvPropertyPrice  = v.findViewById<TextView>(R.id.tv_property_price)
//            val tvPropertyAddress  = v.findViewById<TextView>(R.id.tv_property_address)
//            val tvSqft  = v.findViewById<TextView>(R.id.tv_sqft)
//            val tvParking  = v.findViewById<TextView>(R.id.tv_parking)
//            val tvBath  = v.findViewById<TextView>(R.id.tv_bath)
//            val tvBeds  = v.findViewById<TextView>(R.id.tv_beds)
//            var firstImage = Constants.SUBSTITUTE_IMAGE
//            if(!it.listing_image.isNullOrEmpty())
//                firstImage = getFirstImageFromListingImage(it.listing_image)
//            Picasso.with(context).load(firstImage)
//                    .into(ivProperty)
//
//
////            if(!it.image.isNullOrEmpty())
////                Picasso.with(context).load(it.image)
////                    .into(ivProperty)
////            else
////                Picasso.with(context).load(Constants.SUBSTITUTE_IMAGE)
////                    .into(ivProperty)
//            tvPropertyName.text = context!!.getString(R.string.property_name,it.listing_use)
//            tvPropertyAddress.text = it.listing_address
//            tvPropertyPrice.text = it.listing_price.toString()
//            tvBath.text = context!!.getString(R.string.bath_count,it.listing_baths_count)
//            tvBeds.text = context!!.getString(R.string.bed_count,it.listing_beds_count)
//            tvParking.text = context!!.getString(R.string.parking_count,it.listing_parking_spot)
//            tvSqft.text = context!!.getString(R.string.sqft_count,it.listing_sqft)
//            ll_featured_list_container.addView(v)
//        }
    }

    override fun loadFeatureListDatas(propertyListModel: AllPropertyListModel) {
        this.featuredList = propertyListModel
        displayFeatureistData()
    }

    override fun loadFeatureListFailed(errorMsg: String) {

    }

    override fun loadNewPropertyListSuccess(propertyListModel: AllPropertyListModel) {
        this.newPropertyList = propertyListModel
        displayNewListData()
    }

    private fun displayNewListData() {
        var horizontalLayoutManager = LinearLayoutManager(activity,LinearLayoutManager.VERTICAL,false)
        rv_new_list.layoutManager = horizontalLayoutManager
        var listNewPorpety  = newPropertyList.data
      //  Collections.reverse(listNewPorpety)
        rv_new_list.adapter = PropertyListAdapter(context!!,listNewPorpety,this,activity!!)

    }

    override fun loadNewPropertyListFailed(errorMsg: String) {

    }

    override fun showProgress(show: Boolean) {
    showLoadingProgress(progressBar,show)

    }

    companion object {
        const val TAG = "MainHomeFragment"
    }

    override fun onResume() {
        super.onResume()
        presenter.loadNewPropertyList()
    }

    override fun onFavouriteSelected(status: Boolean, propertyID: Int) {
        var regrexIDCHECK = "+${propertyID}+"
        var favouriteList = mySharedPreferences.getString(Constants.FAVOURITE_LIST)
        if(status)
        {
            favouriteList += regrexIDCHECK
            addFavouriteProperty(propertyID)
            Log.d(Constants.LOG_TAG,"Fav Selcted $propertyID")
        }
        else{
            favouriteList = favouriteList.replace(regrexIDCHECK,"")
            Log.d(Constants.LOG_TAG,"This is favuor $favouriteList")
            removeFavouriteProperty(propertyID)
            Log.d(Constants.LOG_TAG,"Fav Removed $propertyID")
        }
        mySharedPreferences.putString(Constants.FAVOURITE_LIST,favouriteList)
    }

    fun addFavouriteProperty(propertyID: Int){
        var addObservable = api.addLidtFav(propertyID)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({},{
                Log.d(Constants.LOG_TAG,"Add Fav list failed : ${it.localizedMessage}")
            })
        disposable.add(addObservable)
    }
    fun removeFavouriteProperty(propertyID: Int){
        var removeObseravble = api.removeLidtFav(propertyID)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({},{
                Log.d(Constants.LOG_TAG,"Remove Fav list failed : ${it.localizedMessage}")
            })
        disposable.add(removeObseravble)
    }


}
