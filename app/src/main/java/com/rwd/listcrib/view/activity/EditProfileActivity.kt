package com.rwd.listcrib.view.activity

import android.Manifest
import android.app.Dialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import com.carveniche.wisdomleap.di.module.SharedPreferenceModule
import com.rwd.listcrib.R
import com.rwd.listcrib.Util.*
import com.rwd.listcrib.api.ApiInterface
import com.rwd.listcrib.di.component.DaggerActivityComponent
import com.rwd.listcrib.di.module.ActivityModule
import com.rwd.listcrib.di.module.ContextModule
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.layout_progressbar.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import pub.devrel.easypermissions.EasyPermissions
import java.io.File
import java.lang.Exception
import javax.inject.Inject

class EditProfileActivity : AppCompatActivity() {
    @Inject
    lateinit var mySharedPreferences: MySharedPreferences
    private var api = ApiInterface.create()
    private var disposable = CompositeDisposable()
    private var photos = mutableListOf<File>()
    private val RC_CAMERA_PERM = 123
    private val RC_STORAGE_PERM = 124
    private var mUserName = ""
    private var mUserEmail = ""
    private var mUserAddress = ""
    private var mUserContact = ""
    private var mUserImage = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        api = ApiInterface.create(this)
        injectDependency()
        try
        {
            initUI()
        }
        catch(ae : Exception){
            Log.d(Constants.LOG_TAG,"Edit Profile : "+ae.localizedMessage)
        }

    }

    private fun injectDependency() {
        val activityComponent = DaggerActivityComponent.builder()
            .activityModule(ActivityModule(this))
            .sharedPreferenceModule(SharedPreferenceModule())
            .contextModule(ContextModule(this))
            .build()
        activityComponent.inject(this)
    }

    private fun customActionBar() {
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        supportActionBar!!.setDisplayShowCustomEnabled(true);
        supportActionBar!!.setCustomView(R.layout.action_bar_view);
        var view = supportActionBar!!.customView
        var ivActionBack = view.findViewById<ImageView>(R.id.iv_action_back)
        ivActionBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun initUI() {
        customActionBar()
        if(intent.getStringExtra(Constants.USER_NAME) != null)
            mUserName = intent.getStringExtra(Constants.USER_NAME)
        if(intent.getStringExtra(Constants.USER_EMAIL) != null)
            mUserEmail = intent.getStringExtra(Constants.USER_EMAIL)
        if(intent.getStringExtra(Constants.USER_ADDRESS) != null)
            mUserAddress = intent.getStringExtra(Constants.USER_ADDRESS)
        if(intent.getStringExtra(Constants.USER_CONTACT) != null)
            mUserContact = intent.getStringExtra(Constants.USER_CONTACT)
        if(intent.getStringExtra(Constants.USER_IMAGE) != null)
           mUserImage = intent.getStringExtra(Constants.USER_IMAGE)
        iv_camera.setOnClickListener {
            cameraTask()
        }
        tv_update.setOnClickListener {
            updateUserDatas()
        }

        ed_user_name.setText(mUserName)
        ed_email.setText(mUserEmail)
        ed_contact.setText(mUserContact)
        ed_address.setText(mUserAddress)

        if(!mUserImage.isNullOrEmpty())
        {
            Picasso.with(this).load(mUserImage)
                .into(iv_avatar)
        }
        if(mySharedPreferences.getBoolean(Constants.IS_SOCIAL_LOGIN,false))
        {
            tv_change_password.visibility = View.GONE
        }
        else
        {
            tv_change_password.visibility = View.VISIBLE
        }
        tv_change_password.setOnClickListener {
            changePassword()
        }
    }

    private fun changePassword() {
        val dialog = Dialog(this,android.R.style.Theme_Black_NoTitleBar_Fullscreen)
        dialog .requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog .setCancelable(false)
        dialog .setContentView(R.layout.dialog_change_password)
        val tvSubmit = dialog .findViewById(R.id.tv_submit) as TextView
        val tvPassword = dialog.findViewById(R.id.ed_password) as EditText
        val tvClose = dialog.findViewById(R.id.tv_close) as TextView
        val tvConfirmPassword = dialog.findViewById(R.id.ed_confirm_password) as EditText
        tvSubmit.setOnClickListener {
            submitPasswordChange(tvPassword.text.toString(),tvConfirmPassword.text.toString(),dialog)
        }
        tvClose.setOnClickListener {
            dialog.dismiss()
        }
//        val body = dialog .findViewById(R.id.body) as TextView
//        body.text = title
//        val yesBtn = dialog .findViewById(R.id.yesBtn) as Button
//        val noBtn = dialog .findViewById(R.id.noBtn) as TextView
//        yesBtn.setOnClickListener {
//            dialog .dismiss()
//        }
//        noBtn.setOnClickListener { dialog .dismiss() }
        dialog .show()
    }
    private fun submitPasswordChange(pass : String,confirmPass : String,dialog: Dialog){
        if(pass != confirmPass)
        {
            showLongToast("Password does not match",this)
            return
        }
        else{
            showProgress(true)
            var email = mySharedPreferences.getString(Constants.USER_EMAIL)
            Log.d(Constants.LOG_TAG,"EMAIL SHARE "+email)
            var changePassObservable = api.changePassword(email,pass,confirmPass)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress(false)
                    Log.d(Constants.LOG_TAG,it.toString())
                    showLongToast("Password Changed Successfully",this)
                    dialog.dismiss()
                },{
                    showProgress(false)
                    Log.d(Constants.LOG_TAG,it.toString())
                    showLongToast("Password Changed Successfully",this)
                    dialog.dismiss()
                })
            disposable.add(changePassObservable)
        }
    }

    private fun takePhoto() {
        EasyImage.configuration(this)
            .setImagesFolderName("ListCrib")
            .setCopyTakenPhotosToPublicGalleryAppFolder(true)
            .setCopyPickedImagesToPublicGalleryAppFolder(true)
            .setAllowMultiplePickInGallery(true);
        EasyImage.openChooserWithGallery(this,"Choose Image",0)
    }
    fun showProgress(status : Boolean){
        showLoadingProgress(progressBar,status)
    }

    private fun updateUserDatas(){

        mUserName = ed_user_name.text.toString()
        mUserEmail = ed_email.text.toString()
        mUserContact = ed_contact.text.toString()
        mUserAddress = ed_address.text.toString()

        showProgress(true)
        var requestUserName = RequestBody.create(MediaType.parse("text/plain"),mUserName)
        var requestEmail = RequestBody.create(MediaType.parse("text/plain"),mUserEmail)
        var requestContact = RequestBody.create(MediaType.parse("text/plain"),mUserContact)
        var requestAddress = RequestBody.create(MediaType.parse("text/plain"),mUserAddress)

        if(photos.isNotEmpty()){
            var file = File(photos[0].path)
            var surveyBody = RequestBody.create(MediaType.parse("image/*"), file)
            var requestImage = MultipartBody.Part.createFormData("f_image",file.name,surveyBody)
            var regObservable = api.userUpdateWithImage(requestUserName,requestEmail,requestContact,requestAddress,requestImage)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress(false)
                    if(it.errors.isNullOrEmpty())
                    {
                        showShortToast(Constants.PROFILE_UPDATE_MESSAGE,this)
                        goToMainActivity(this)
                    }
                    else{
                        toastErrorMessage(this,it.errors)
                    }


                },{
                    showProgress(false)
                    Log.d(Constants.LOG_TAG,it.localizedMessage)
                })

            disposable.add(regObservable)
        }
        else
        {
            var regObservable = api.userUpdate(requestUserName,requestEmail,requestContact,requestAddress)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    showProgress(false)
                    if(it.errors.isNullOrEmpty())
                    {
                        showShortToast(Constants.PROFILE_UPDATE_MESSAGE,this)
                        goToMainActivity(this)
                    }
                    else{
                        toastErrorMessage(this,it.errors)
                    }


                },{
                    showProgress(false)
                    Log.d(Constants.LOG_TAG,it.localizedMessage)
                })

            disposable.add(regObservable)
        }


    }



    private fun cameraTask(){
        if(hasCameraPermission(this) && hasWriteStoragePermission(this)){
            takePhoto()
        }
        else
        {
            if(!hasCameraPermission(this))
            {
                requestCameraPermission()
            }
            if(!hasWriteStoragePermission(this)){
                requestStoragePermission()
            }

        }
    }

    fun requestCameraPermission(){
        EasyPermissions.requestPermissions(
            this,
            getString(R.string.rationale_camera),
            RC_CAMERA_PERM,
            Manifest.permission.CAMERA)
    }
    fun requestStoragePermission(){
        EasyPermissions.requestPermissions(
            this,
            getString(R.string.rationale_camera),
            RC_CAMERA_PERM,
            Manifest.permission.WRITE_EXTERNAL_STORAGE)
    }

    fun onPhotosReturned(returnedPhotos : List<File>){
        photos.addAll(returnedPhotos)

        displayAddedImages()
    }
    private fun displayAddedImages() {
        Log.d(Constants.LOG_TAG,photos[0].path)
        var imgBitmap = getBitmapFromPath(photos[0].path)
        iv_avatar.setImageBitmap(imgBitmap)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, object : DefaultCallback() {
            override fun onImagesPicked(p0: MutableList<File>, p1: EasyImage.ImageSource?, p2: Int) {
                Log.d(Constants.LOG_TAG,"p0 : $p0 p1: $p1 p2 : $p2")

                onPhotosReturned(p0)

            }

            override fun onCanceled(source: EasyImage.ImageSource?, type: Int) {
                super.onCanceled(source, type)
            }

            override fun onImagePickerError(e: Exception?, source: EasyImage.ImageSource?, type: Int) {
                super.onImagePickerError(e, source, type)
                Log.e(Constants.LOG_TAG,e.toString())
            }

        })
    }
}
