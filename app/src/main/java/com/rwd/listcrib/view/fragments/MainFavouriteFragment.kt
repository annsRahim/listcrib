package com.rwd.listcrib.view.fragments


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.carveniche.wisdomleap.di.module.SharedPreferenceModule
import com.rwd.listcrib.R
import com.rwd.listcrib.Util.Constants
import com.rwd.listcrib.Util.MySharedPreferences
import com.rwd.listcrib.adapter.FavouritePropertyListAdapter
import com.rwd.listcrib.adapter.PropertyListAdapter
import com.rwd.listcrib.adapter.SearchPropertyListAdapter
import com.rwd.listcrib.api.ApiInterface
import com.rwd.listcrib.contract.MainFavouritesContract
import com.rwd.listcrib.di.component.DaggerFragmentComponent
import com.rwd.listcrib.di.module.ContextModule
import com.rwd.listcrib.di.module.FragmentModule
import com.rwd.listcrib.interfaces.IFavouriteSelectedListener
import com.rwd.listcrib.models.propertListModel.AllPropertyListModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_main_favourites.*
import javax.inject.Inject


class MainFavouriteFragment : Fragment(),MainFavouritesContract.View,IFavouriteSelectedListener {

    @Inject lateinit var mySharedPreferences: MySharedPreferences
    private lateinit var rootView: View
    private lateinit var newPropertyList  : AllPropertyListModel
    private var api  = ApiInterface.create()
    private var disposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependency()
        api = ApiInterface.create(context!!)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_main_favourites, container, false)
        return rootView
    }
    companion object {
        const val TAG = "MainFavouriteFragment"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
    }

    private fun initUI() {

       loadPropertyList()
    }

    private fun loadPropertyList() {
        var listDisposable = api.listAllFavourites()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Log.d(Constants.LOG_TAG,it.toString())
                onListLoadDataSuccess(it)
            },{
                Log.d(Constants.LOG_TAG,it.localizedMessage)
                onListLoadDataFailed(it.localizedMessage)
            })
        disposable.add(listDisposable)
    }

    private fun injectDependency() {
        val fragmentComponent = DaggerFragmentComponent.builder()
            .fragmentModule(FragmentModule())
            .sharedPreferenceModule(SharedPreferenceModule())
            .contextModule(ContextModule(context!!))
            .build()
        fragmentComponent.inject(this)

    }

    private fun displayDatas() {
        rv_new_list.layoutManager = LinearLayoutManager(context!!);
        rv_new_list.adapter = PropertyListAdapter(context!!,newPropertyList.data,this,activity!!)
        if(newPropertyList.data.isEmpty())
            tv_no_data_found.visibility = View.VISIBLE
        else
            tv_no_data_found.visibility = View.GONE
    }

    override fun onListLoadDataSuccess(propertyListModel: AllPropertyListModel) {
        this.newPropertyList = propertyListModel
        Log.d(Constants.LOG_TAG,"Favorites")
        Log.d(Constants.LOG_TAG,newPropertyList.toString())
        Log.d(Constants.LOG_TAG,"Fav")
        displayDatas()
    }

    override fun onListLoadDataFailed(msg: String) {

    }

    override fun showProgress(show: Boolean) {

    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        Log.d(Constants.LOG_TAG,"Is Visible $isVisibleToUser")
        if(isVisibleToUser)
        {
            loadPropertyList()
        }
    }

    override fun onFavouriteSelected(status: Boolean, propertyID: Int) {
        var regrexIDCHECK = "+${propertyID}+"
        var favouriteList = mySharedPreferences.getString(Constants.FAVOURITE_LIST)
        if(status)
        {
            favouriteList += regrexIDCHECK
            addFavouriteProperty(propertyID)

            Log.d(Constants.LOG_TAG,"Fav Selcted $propertyID")
        }
        else{
            favouriteList = favouriteList.replace(regrexIDCHECK,"")
            Log.d(Constants.LOG_TAG,"This is favuor $favouriteList")
            removeFavouriteProperty(propertyID)
            Log.d(Constants.LOG_TAG,"Fav Removed $propertyID")
        }
        mySharedPreferences.putString(Constants.FAVOURITE_LIST,favouriteList)
        loadPropertyList()
    }

    fun addFavouriteProperty(propertyID: Int){
        var addObservable = api.addLidtFav(propertyID)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({},{
                Log.d(Constants.LOG_TAG,"Add Fav list failed : ${it.localizedMessage}")
            })
        disposable.add(addObservable)
    }
    fun removeFavouriteProperty(propertyID: Int){
        var removeObseravble = api.removeLidtFav(propertyID)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({},{
                Log.d(Constants.LOG_TAG,"Remove Fav list failed : ${it.localizedMessage}")
            })
        disposable.add(removeObseravble)
    }


}
