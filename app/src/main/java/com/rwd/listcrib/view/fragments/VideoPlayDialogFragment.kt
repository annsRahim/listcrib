package com.rwd.listcrib.view.fragments

import android.app.Dialog
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.extractor.ExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelection
import com.google.android.exoplayer2.trackselection.TrackSelector
import com.google.android.exoplayer2.upstream.BandwidthMeter
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.rwd.listcrib.R
import kotlinx.android.synthetic.main.dialog_video_play.view.*


class VideoPlayDialogFragment(var mContext : Context,var videoUrl : String) : DialogFragment() {
    private lateinit var rootView : View
    private lateinit var player : ExoPlayer
    private var videoDemoUrl = "http://res.cloudinary.com/lucajax/video/upload/v1583941226/kwav2gtomk6pysiecgow.mov"

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogStyle);
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.dialog_video_play,container,false)
        return rootView
    }

    override fun onStart() {
        super.onStart()
        initializePlayer()
    }

    private fun initializePlayer() {
        val bandwidthMeter: BandwidthMeter = DefaultBandwidthMeter()
        val videoTrackSelectionFactory: TrackSelection.Factory =
            AdaptiveTrackSelection.Factory(bandwidthMeter)
        val trackSelector: TrackSelector = DefaultTrackSelector(videoTrackSelectionFactory)
         player = ExoPlayerFactory.newSimpleInstance(mContext,trackSelector)
        rootView.exo_player.player = player

        val dataSourceFactory: DataSource.Factory =
            DefaultDataSourceFactory(mContext, Util.getUserAgent(mContext, "CloudinaryExoplayer"))

        // Produces Extractor instances for parsing the media data.
        // Produces Extractor instances for parsing the media data.
        val extractorsFactory: ExtractorsFactory = DefaultExtractorsFactory()

        // This is the MediaSource representing the media to be played.
        // This is the MediaSource representing the media to be played.
        val videoUri: Uri = Uri.parse(videoUrl)
        val videoSource: MediaSource = ExtractorMediaSource(
            videoUri,
            dataSourceFactory, extractorsFactory, null, null
        )

        // Prepare the player with the source.
        // Prepare the player with the source.
        player.prepare(videoSource)
        player.playWhenReady = true
    }

    override fun onPause() {
        super.onPause()

    }

    override fun onDestroy() {
        super.onDestroy()
        releasePlayer()
    }

    private fun releasePlayer() {
        if (player != null) {
            player.stop();
            player.release();

        }
    }
}