package com.rwd.listcrib.view.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager

import com.rwd.listcrib.R
import com.rwd.listcrib.Util.showLoadingProgress
import com.rwd.listcrib.Util.showShortToast
import com.rwd.listcrib.adapter.ChatMessageListAdapter
import com.rwd.listcrib.api.ApiInterface
import com.rwd.listcrib.models.ChatItemModel
import com.rwd.listcrib.models.ChatListModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_chat_message.*
import kotlinx.android.synthetic.main.layout_progressbar.*
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class ChatMessageFragment : Fragment() {

    private lateinit var rootView: View
    private var api = ApiInterface.create()
    private var disposable = CompositeDisposable()
    private lateinit var chatListModel: ChatListModel
    private var chatItemList = mutableListOf<ChatItemModel>()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        api = ApiInterface.create(context!!)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_chat_message, container, false)
        return rootView
    }

     fun showProgress(show: Boolean) {
        showLoadingProgress(progressBar,show)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadChatList()
        var verticalLayoutManager = LinearLayoutManager(activity,LinearLayoutManager.VERTICAL,false)
        rv_chat_message_list.layoutManager = verticalLayoutManager
    }

    private fun loadChatList() {
        showProgress(true)
        var chatObseravble = api.getChatList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress(false)
                if(it.status == 200){
                    this.chatListModel = it
                    displayChatList()
                }
                else
                {
                    showShortToast("Unable to load Chat please try again",context!!)
                }
            },{
                showProgress(false)
                showShortToast("Unable to load Chat please try again",context!!)
            })
        disposable.add(chatObseravble)
    }

    private fun displayChatList() {
        chatItemList.clear()
        chatListModel.fromchat.forEach {
            var chatItemModel = ChatItemModel(it.from_user_name,it.to_user_name,it.tou_image,it.sid,it.updated_at)
            chatItemList.add(chatItemModel)
        }
        chatListModel.tochat.forEach {
            var chatItemModel = ChatItemModel(it.from_user_name,it.to_user_name,it.tou_image,it.sid,it.updated_at)
            chatItemList.add(chatItemModel)
        }
        Collections.reverse(chatItemList)
        rv_chat_message_list.adapter = ChatMessageListAdapter(context!!,chatItemList)
    }

}
