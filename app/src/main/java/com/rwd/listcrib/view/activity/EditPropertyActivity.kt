package com.rwd.listcrib.view.activity

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.Toast
import com.carveniche.wisdomleap.di.module.SharedPreferenceModule
import com.rwd.listcrib.R
import com.rwd.listcrib.di.component.DaggerActivityComponent
import com.rwd.listcrib.di.module.ActivityModule
import com.rwd.listcrib.di.module.ContextModule

import pl.aprilapps.easyphotopicker.EasyImage
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions
import javax.inject.Inject
import java.nio.file.Files.delete
import pl.aprilapps.easyphotopicker.DefaultCallback
import android.R.attr.data
import android.app.Activity
import android.content.Context
import android.os.AsyncTask
import android.os.Environment
import android.provider.MediaStore
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBar

import com.rwd.listcrib.Util.*
import com.rwd.listcrib.api.ApiInterface
import com.rwd.listcrib.models.DetailData
import com.squareup.picasso.Picasso
import com.vincent.videocompressor.VideoCompress
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

import kotlinx.android.synthetic.main.edit_list_property_detail.*

import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.Multipart

import java.io.File
import java.io.Serializable
import java.lang.Exception





class EditPropertyActivity : AppCompatActivity(),EasyPermissions.PermissionCallbacks {

    private val PHOTOS_KEY = "easy_image_photos_list"
    private var photos = mutableListOf<File>()
    private var cameraVideos = mutableListOf<File>()
    private var api = ApiInterface.create()
    private var disposable = CompositeDisposable()
    private val RC_CAMERA_PERM = 123
    private val RC_AUDIO_PERM = 124
    @Inject lateinit var mySharedPreferences: MySharedPreferences
    private var mListTitle = ""
    private var mListPrice = ""
    private var mListAddress = ""
    private var mListType = ""
    private var mListUse = ""
    private var mListSqft = ""
    private var mListBath = ""
    private var mListBeds = ""
    private var mListParking = ""
    private var mListDescription = ""
    private var validationErrorMessage = ""
    private var mPropertyId = 0
    private lateinit var detailData: DetailData
    private var pathToStoredVideo = ""
    private val REQUEST_VIDEO_CAPTURE = 1255

    private val perms = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CALL_PHONE)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_list_property_detail)
        injectDependency()
        if(savedInstanceState!=null){
            photos = savedInstanceState.getSerializable(PHOTOS_KEY) as MutableList<File>
        }
        api = ApiInterface.create(this)
        mPropertyId = intent.getIntExtra(Constants.PROPERTY_ID,0)
        initUI()
    }
    private fun showProgress(status : Boolean){
        showLoadingProgress(progressBarSubmit,status)
    }

    private fun customActionBar() {
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        supportActionBar!!.setDisplayShowCustomEnabled(true);
        supportActionBar!!.setCustomView(R.layout.action_bar_view);
        var view = supportActionBar!!.customView
        var ivActionBack = view.findViewById<ImageView>(R.id.iv_action_back)
        ivActionBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun initUI() {
        customActionBar()
        setListType()
        setSaleType()
        tv_take_photo.setOnClickListener {
            cameraTask()
        }
        tv_record_video.setOnClickListener {
            videoTask()
        }
        tv_choose_file.setOnClickListener {
            pickFromGallery()
        }
        tv_choose_file_video.setOnClickListener {
            if(cameraVideos.isEmpty())
                pickVideoFromGallery()
        }
        displayAddedImages()
        btn_save.setOnClickListener {

            if(isValidationSuccess())
                submitProperty()
            else
                showLongToast(validationErrorMessage,this)
        }
        loadDetailDatas()
    }
    private fun pickVideoFromGallery() {
        var intent = Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
//        intent.setType("video/*")
//        intent.setAction(Intent.ACTION_GET_CONTENT)
        startActivityForResult(intent,REQUEST_VIDEO_CAPTURE)
    }

    private fun loadDetailDatas() {
        showProgress(true)
        var detailObservable = api.propertyDetail(mPropertyId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress(false)
                Log.d(Constants.LOG_TAG,"Property details")
                if(it.status == 200)
                {
                    this.detailData = it.data
                    displayDatas(it.data)
                }
                else{
                    finish()
                }
                Log.d(Constants.LOG_TAG,it.toString())
            },{
                showProgress(false)
               finish()

            })
        disposable.add(detailObservable)
    }

    private fun displayDatas(data: DetailData) {
        try{
            if(!data.listing_title.isNullOrEmpty())
                ed_title.setText(data.listing_title)
            if(data.listing_price != null)
                ed_price.setText(data.listing_price.toString())
            if(!data.listing_address.isNullOrEmpty())
                ed_address.setText(data.listing_address.toString())
//            if(!data.listing_sqft.isNullOrEmpty())
//                ed_sqft.setText(data.listing_sqft.toString())
//            if(data.listing_beds_count != null)
//                ed_beds.setText(data.listing_beds_count)
//            if(data.listing_baths_count != null)
//                ed_bath.setText(data.listing_baths_count)
//            if(data.listing_parking_spot != null)
//                ed_parking.setText(data.listing_parking_spot)
            if(data.listing_description != null)
                ed_description.setText(data.listing_description.toString())

            if(data.listing_type != null)
            {
              Log.d(Constants.LOG_TAG,data.listing_type+" -- "+Constants.LISTING_TYPE.indexOf(data.listing_type).toString())
               var position = Constants.LISTING_TYPE.indexOf(data.listing_type)
                if(position>=0)
                    spinner_type.setSelection(position)
            }

            if(data.listing_use != null){
                Log.d(Constants.LOG_TAG,data.listing_use+" -USE- "+Constants.LISTING_USE.indexOf(data.listing_use).toString())
                var position = Constants.LISTING_USE.indexOf(data.listing_use)
                if(position>=0)
                    spinner_sale.setSelection(position)
            }
        }
        catch (ae : Exception){
            Log.e(Constants.LOG_TAG,ae.localizedMessage+" Edit Display data Error")
        }

    }

    private fun isValidationSuccess(): Boolean {
        mListTitle = ed_title.text.toString()
        mListPrice = ed_price.text.toString()
        mListAddress = ed_address.text.toString()
        mListType = spinner_type.selectedItem.toString()
        mListUse = spinner_sale.selectedItem.toString()
        mListSqft = ed_sqft.text.toString()
        mListBeds = ed_beds.text.toString()
        mListBath = ed_bath.text.toString()
        mListParking = ed_parking.text.toString()
        mListDescription = ed_description.text.toString()
        var validResult = true
            validationErrorMessage = "Following fields are required "
        if(mListTitle.isEmpty() ){
            validationErrorMessage+= " \n Title"
            validResult = false
        }
        if(mListPrice.isEmpty() ){
            validationErrorMessage+= " \n Price"
            validResult = false
        }
        if(mListAddress.isEmpty() ){
            validationErrorMessage+= " \n Address"
            validResult = false
        }
//        if(mListSqft.isEmpty() ){
//            validationErrorMessage+= " \n Sqft"
//            validResult = false
//        }
//        if(mListBeds.isEmpty() ){
//            validationErrorMessage+= " \n Bed Count"
//            validResult = false
//        }
//        if(mListBath.isEmpty() ){
//            validationErrorMessage+= " \n Baths"
//            validResult = false
//        }
//        if(mListParking.isEmpty() ){
//            validationErrorMessage+= " \n Parking"
//            validResult = false
//        }
        if(mListDescription.isEmpty() ){
            validationErrorMessage+= " \n Description"
            validResult = false
        }

        if(mListType == Constants.SELECT || mListUse == Constants.SELECT)
        {
            validationErrorMessage+= " \n List Type \n List Use"
            validResult = false
        }


        return validResult



    }

    private fun videoTask() {
        if(hasVideoAudioPermission() && hasCameraPermission() && hasWriteStoragePermission()){
            try{
                    takeVideo()
            }
            catch (ae : Exception){
                Log.d(Constants.LOG_TAG,"Video Exception ${ae.localizedMessage}")
            }

        }
        else
        {
            if(!hasCameraPermission())
            {
                requestCameraPermission()
            }
            if(!hasVideoAudioPermission()){
                requestVideoAudioPermission()
            }
            if(!hasWriteStoragePermission()){
                requestStoragePermission()
            }

        }
    }
    fun requestVideoAudioPermission(){
        EasyPermissions.requestPermissions(
            this,
            getString(R.string.rationale_audio),
            RC_AUDIO_PERM,
            Manifest.permission.RECORD_AUDIO)
    }

    private fun takeVideo() {
        var videoCaptureIntent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
        videoCaptureIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY,0)
        videoCaptureIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT,60)
        if(videoCaptureIntent.resolveActivity(getPackageManager()) != null){
            startActivityForResult(videoCaptureIntent, REQUEST_VIDEO_CAPTURE);
        }
    }

    private fun pickFromGallery(){
        EasyImage.openChooserWithGallery(this,"Select Image",0)
    }

    private fun displayAddedImages() {
        ll_added_image_container.removeAllViews()
        photos.forEach {
            var textView = TextView(this)
            textView.text = it.name
            ll_added_image_container.addView(textView)
        }
    }

    private fun injectDependency() {
        val activityComponent = DaggerActivityComponent.builder()
            .activityModule(ActivityModule(this))
            .sharedPreferenceModule(SharedPreferenceModule())
            .contextModule(ContextModule(this))
            .build()
        activityComponent.inject(this)
    }
    private fun setListType() {
        var listTypeAdapter = ArrayAdapter<String>(this,   android.R.layout.simple_spinner_item, Constants.LISTING_TYPE)
        listTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinner_type.adapter = listTypeAdapter;
    }
    private fun setSaleType() {
        var listTypeAdapter = ArrayAdapter<String>(this,   android.R.layout.simple_spinner_item, Constants.LISTING_USE)
        listTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinner_sale.adapter = listTypeAdapter;
    }

    private fun hasCameraPermission():Boolean {
        return EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA)
    }
    private fun hasVideoAudioPermission():Boolean {
        return EasyPermissions.hasPermissions(this, Manifest.permission.RECORD_AUDIO)
    }
    private fun hasWriteStoragePermission():Boolean {
        return EasyPermissions.hasPermissions(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    }


    private fun submitProperty(){
        showProgress(true)
        var requestTitle =  RequestBody.create(MediaType.parse("multipart/form-data"), mListTitle);
        var requestPrice =  RequestBody.create(MediaType.parse("multipart/form-data"), mListPrice);
        var requestAddress =  RequestBody.create(MediaType.parse("multipart/form-data"), mListAddress);
        var requestListType=  RequestBody.create(MediaType.parse("multipart/form-data"), mListType);
        var requestListUse =  RequestBody.create(MediaType.parse("multipart/form-data"), mListUse);
        var requestListSqft =  RequestBody.create(MediaType.parse("multipart/form-data"), mListSqft);
        var requestListBeds =  RequestBody.create(MediaType.parse("multipart/form-data"), mListBeds);
        var requestListSpot =  RequestBody.create(MediaType.parse("multipart/form-data"), mListParking);
        var requestListBaths =  RequestBody.create(MediaType.parse("multipart/form-data"), mListBath);
        var requestListDesc =  RequestBody.create(MediaType.parse("multipart/form-data"), mListDescription);
        var requestImageList  = mutableListOf<MultipartBody.Part>()


        try{
            photos.forEach {
                Log.d("Add Image",it.name)
                var file = File(it.path)
                var surveyBody = RequestBody.create(MediaType.parse("image/*"), file)
                var surveyForm = MultipartBody.Part.createFormData("property_image",file.name,surveyBody)
                requestImageList.add(surveyForm)
            }
            var videoFile = File(pathToStoredVideo)


            if(videoFile.exists())
            {
                var surveyBody = RequestBody.create(MediaType.parse("video/mp4"), videoFile)
                var requestVideo = MultipartBody.Part.createFormData("property_video",videoFile.name,surveyBody)
                var submitObservable = api.addProperty( requestTitle,
                    requestPrice,
                    requestAddress,
                    requestListType,
                    requestListUse,
                    requestListSqft,
                    requestListBeds,
                    requestListSpot,
                    requestListBaths,
                    requestListDesc,
                    requestImageList,
                    requestVideo
                )
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        showProgress(false)
                        showShortToast("Property Added Successfully",this)
                        goToMainActivity()
                        Log.d(Constants.LOG_TAG,it.toString())
                    },{
                        showProgress(false)
                        Log.e(Constants.LOG_TAG,it.localizedMessage)
                    })
                disposable.add(submitObservable)
            }
            else
            {
                var submitObservable = api.addPropertyWithoutVideo( requestTitle,
                    requestPrice,
                    requestAddress,
                    requestListType,
                    requestListUse,
                    requestListSqft,
                    requestListBeds,
                    requestListSpot,
                    requestListBaths,
                    requestListDesc,
                    requestImageList
                )
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        showProgress(false)
                        showShortToast("Property Added Successfully",this)
                        goToMainActivity()
                        Log.d(Constants.LOG_TAG,it.toString())
                    },{
                        showProgress(false)
                        Log.e(Constants.LOG_TAG,it.localizedMessage)
                    })
                disposable.add(submitObservable)
            }




        }
        catch (ae : Exception){
            Log.e(Constants.LOG_TAG,ae.localizedMessage)
            showProgress(true)
        }

    }

    private fun goToMainActivity() {
      var intent = Intent(this,MainActivity::class.java)
        startActivity(intent)
    }

    private fun takePhoto(){
        EasyImage.configuration(this)
            .setImagesFolderName("ListCrib")
            .setCopyTakenPhotosToPublicGalleryAppFolder(true)
            .setCopyPickedImagesToPublicGalleryAppFolder(true)
            .setAllowMultiplePickInGallery(true);
        if(photos.size < 6)
        {
            EasyImage.openCameraForImage(this,0)
        }
        else
            showShortToast("You can post maximum of 6 images",this)
    }

    private fun cameraTask(){
        if(hasCameraPermission() && hasWriteStoragePermission()){
            takePhoto()
        }
        else
        {
            if(!hasCameraPermission())
            {
                requestCameraPermission()
            }
            if(!hasWriteStoragePermission()){
               requestStoragePermission()
            }

        }
    }

    fun requestCameraPermission(){
        EasyPermissions.requestPermissions(
            this,
            getString(R.string.rationale_camera),
            RC_CAMERA_PERM,
            Manifest.permission.CAMERA)
    }
    fun requestStoragePermission(){
        EasyPermissions.requestPermissions(
            this,
            getString(R.string.rationale_camera),
            RC_CAMERA_PERM,
            Manifest.permission.WRITE_EXTERNAL_STORAGE)
    }
    override fun onRequestPermissionsResult(requestCode:Int,
                                            permissions:Array<String>,
                                            grantResults:IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        Log.d(Constants.LOG_TAG, "onPermissionsDenied:" + requestCode + ":" + perms.size)

        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms))
        {
            AppSettingsDialog.Builder(this).build().show()
        }
    }

    fun onCustomVideosReturned(){
        showShortToast("Video compression in progress",this)
        showProgress(true)
        Log.d(Constants.LOG_TAG,"Video Acrchive in progress")
        var file = File(pathToStoredVideo)
        var sizeInMb = file.length()/1024
        var time= System.currentTimeMillis();
        Log.d(Constants.LOG_TAG,"Before Compress file size "+sizeInMb+" -- "+file.path)
        var pathUrl = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).absolutePath +"/Camera/VIDEO_"+time+".mp4";
        var destinationPath = File(pathUrl);

        VideoCompress.compressVideoLow(file.path,destinationPath.path,object: VideoCompress.CompressListener{
            override fun onSuccess() {
                showProgress(false)
                var compressedFile = File(destinationPath.path)
                var sizeInMb = compressedFile.length()/1024
                Log.d(Constants.LOG_TAG,"After Compression File Size "+sizeInMb)
                Log.d(Constants.LOG_TAG,"Video Compression Success")
                pathToStoredVideo = destinationPath.path
                displayAddedVideos()
            }

            override fun onFail() {
                showProgress(false)
                Log.d(Constants.LOG_TAG,"Video Compression Failed")
            }

            override fun onProgress(percent: Float) {

                Log.d(Constants.LOG_TAG,"Video Compression progress "+percent)
            }

            override fun onStart() {

                Log.d(Constants.LOG_TAG,"Video Compression Started ")
            }

        })
        //    displayAddedVideos()
    }

    inner class DoAsyncVideoCompress(val mContext : Context) : AsyncTask<String, String, String>() {


        override fun doInBackground(vararg params: String?): String {
            var filePath = ""
            Log.d(Constants.LOG_TAG,"Params "+params[0]+" - "+params[1])
            try{
               // filePath = SiliCompressor.with(mContext).compressVideo(params[0], params[1]);
            }
            catch (ae : Exception){
                Log.e(Constants.LOG_TAG,"Exception In compression ${ae.localizedMessage}")
            }

            return filePath
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            pathToStoredVideo = result!!
            var file = File(result)
            var sizeInMb = file.length()/1024
            Log.d(Constants.LOG_TAG,"After Compress file size "+sizeInMb+" -- path --"+file.path)
            //displayAddedVideos()
            Log.d(Constants.LOG_TAG,"COmpressed Done $result")
        }


    }



    @SuppressLint("StringFormatMatches")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE)
        {
            val yes = getString(R.string.yes)
            val no = getString(R.string.no)
            // Do something after user returned from app settings screen, like showing a Toast.
//            Toast.makeText(
//                this,
//                getString(R.string.returned_from_app_settings_to_activity,
//                    if (hasCameraPermission()) yes else no
//                    ),
//                Toast.LENGTH_LONG)
//                .show()
        }
        if(resultCode == Activity.RESULT_OK && requestCode == REQUEST_VIDEO_CAPTURE){
            pathToStoredVideo = getRealPathFromURIPath(data!!.data,this)!!
            onCustomVideosReturned()
        }

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, object : DefaultCallback() {
            override fun onImagesPicked(p0: MutableList<File>, p1: EasyImage.ImageSource?, p2: Int) {
                Log.d(Constants.LOG_TAG,"p0 : $p0 p1: $p1 p2 : $p2")
                if(p1.toString() != Constants.CAMERA_VIDEO)
                    onPhotosReturned(p0)
                else
                    onVideosReturned(p0)
            }

            override fun onCanceled(source: EasyImage.ImageSource?, type: Int) {
                super.onCanceled(source, type)
            }

            override fun onImagePickerError(e: Exception?, source: EasyImage.ImageSource?, type: Int) {
                super.onImagePickerError(e, source, type)
                Log.e(Constants.LOG_TAG,e.toString())
            }

        })


    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable(PHOTOS_KEY,photos as Serializable)
    }

    fun onPhotosReturned(returnedPhotos : List<File>){
        photos.addAll(returnedPhotos)

        displayAddedImages()
    }

    fun onVideosReturned(returnedVideo : List<File>)
    {
//        cameraVideos.addAll(returnedVideo)
//        displayAddedVideos()

    }

    private fun displayAddedVideos() {
        ll_added_video_container.removeAllViews()
        showShortToast("Video Added successfully",this)
        var textView = TextView(this)
        var file = File(pathToStoredVideo)
        Log.d(Constants.LOG_TAG,"File Size "+file.length()/1024)
        textView.text = file.name
        ll_added_video_container.addView(textView)
    }

    override fun onDestroy() {
        EasyImage.clearConfiguration(this)
        super.onDestroy()

    }


    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        if(requestCode==RC_CAMERA_PERM)
        {
            cameraTask()
        }
        else if(requestCode==RC_AUDIO_PERM)
        {
            videoTask()
        }

    }
}
