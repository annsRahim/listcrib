package com.rwd.listcrib.view.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import com.carveniche.wisdomleap.di.module.SharedPreferenceModule
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import com.rwd.listcrib.R
import com.rwd.listcrib.Util.Constants
import com.rwd.listcrib.Util.MySharedPreferences
import com.rwd.listcrib.di.component.DaggerActivityComponent
import com.rwd.listcrib.di.module.ActivityModule
import com.rwd.listcrib.di.module.ContextModule
import com.rwd.listcrib.view.fragments.LoginFragment
import com.rwd.listcrib.view.fragments.RegisterUserFragment
import javax.inject.Inject


class LoginActivity : AppCompatActivity() {

    @Inject lateinit var mySharedPreferences: MySharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        injectDependency()
        initializeFacebook()


        showLoginFragment()
    customActionBar()
    }
    private fun customActionBar() {
        supportActionBar!!.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        supportActionBar!!.setDisplayShowCustomEnabled(true);
        supportActionBar!!.setCustomView(R.layout.action_bar_view);
        var view = supportActionBar!!.customView
        var ivActionBack = view.findViewById<ImageView>(R.id.iv_action_back)
        ivActionBack.visibility = View.GONE

    }

    private fun initializeFacebook() {
        try{
            FacebookSdk.sdkInitialize(getApplicationContext());
            AppEventsLogger.activateApp(application);
        }
        catch (ae : Exception){
            Log.e(Constants.LOG_TAG,"Facebook init Exception "+ae.localizedMessage)
        }

    }

    fun showLoginFragment() {
        supportFragmentManager.beginTransaction()
            .addToBackStack(null)
            .replace(R.id.frame,LoginFragment(),LoginFragment.TAG)
            .commit()
    }

     fun showRegisterFragment(){
        supportFragmentManager.beginTransaction()
            .addToBackStack(null)
            .replace(R.id.frame,RegisterUserFragment(),RegisterUserFragment.TAG)
            .commit()
    }

    private fun injectDependency() {
        val activityComponent = DaggerActivityComponent.builder()
            .activityModule(ActivityModule(this))
            .sharedPreferenceModule(SharedPreferenceModule())
            .contextModule(ContextModule(this))
            .build()
        activityComponent.inject(this)
    }

    override fun onBackPressed() {

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(Constants.LOG_TAG,"On activity result register photo")
        for (fragment in supportFragmentManager.fragments) {
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }
}
