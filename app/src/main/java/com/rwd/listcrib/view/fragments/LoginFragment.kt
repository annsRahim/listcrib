package com.rwd.listcrib.view.fragments


import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import com.carveniche.wisdomleap.di.module.SharedPreferenceModule
import com.facebook.*
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task

import com.rwd.listcrib.R
import com.rwd.listcrib.Util.*
import com.rwd.listcrib.api.ApiInterface
import com.rwd.listcrib.di.component.DaggerFragmentComponent
import com.rwd.listcrib.di.module.ContextModule
import com.rwd.listcrib.di.module.FragmentModule
import com.rwd.listcrib.models.GoogleLoginModel
import com.rwd.listcrib.models.NewUserRegisterModel
import com.rwd.listcrib.view.activity.LoginActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_login.*

import kotlinx.android.synthetic.main.layout_progressbar.*
import okhttp3.MediaType
import okhttp3.RequestBody
import java.lang.Exception
import javax.inject.Inject

import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.rwd.listcrib.models.LoginUserModel
import org.json.JSONObject
import java.util.*


class LoginFragment : Fragment() {

    private lateinit var loginActivity: LoginActivity
    private lateinit var rootView : View
    @Inject
    lateinit var mySharedPreferences: MySharedPreferences
    private var disposable = CompositeDisposable()
    private var api = ApiInterface.create()
    private var mUserName = ""
    private var mEmail = ""
    private var mGoogleToken = ""
    private var mAddress = ""
    private var mProfileImageUrl = ""
    private var mPassword = ""
    private lateinit var mGoogleSignInClient : GoogleSignInClient
    private var RC_GOOGLE_SIGN_IN = 123
    private lateinit var callBackManager : CallbackManager
    private lateinit var loginButton: LoginButton

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_login, container, false)
        return rootView
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        //printHashKey(context!!)
        configureGoogleSiginIn()
        Log.d(Constants.LOG_TAG,"Facebook access token "+AccessToken.getCurrentAccessToken())
        var isLoggedOut = AccessToken.getCurrentAccessToken() == null
        if(!isLoggedOut)
        {
            getUserProfile(AccessToken.getCurrentAccessToken())
        }

    }

    private fun getUserProfile(currentAccessToken: AccessToken?) {
        var request = GraphRequest.newMeRequest(currentAccessToken, object : GraphRequest.GraphJSONObjectCallback{
            override fun onCompleted(`object`: JSONObject?, response: GraphResponse?) {
                try{
                    Log.d(Constants.LOG_TAG,`object`!!.getString("email"))
                    Log.d(Constants.LOG_TAG,`object`!!.getString("first_name"))
                    Log.d(Constants.LOG_TAG,`object`!!.getString("last_name"))
                    Log.d(Constants.LOG_TAG,`object`!!.getString("id"))

                    mUserName = `object`!!.getString("first_name") +" "+`object`!!.getString("last_name")
                    mEmail = `object`!!.getString("email")
                    mGoogleToken = `object`!!.getString("id")
                    submitRegisterDatas()


                }
                catch (ae : Exception){
                    showShortToast("Unable to login with facebook : "+ae.localizedMessage,context!!)
                    Log.e(Constants.LOG_TAG,"Faceook Access token fetch data error : "+ae.localizedMessage)
                }

            }

        })
        var parameters = Bundle()
        parameters.putString("fields", "first_name,last_name,email,id")
        request.parameters = parameters
        request.executeAsync()
    }

    private fun configureFacebookSignIn() {
        callBackManager  = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callBackManager,object : FacebookCallback<LoginResult>{
            override fun onSuccess(result: LoginResult?) {
                Log.d(Constants.LOG_TAG,"Facebook login success "+result)
                getUserProfile(result!!.accessToken)
            }

            override fun onCancel() {

                Log.d(Constants.LOG_TAG,"Facebook login cancelled... ")
            }

            override fun onError(error: FacebookException?) {
                showLongToast("Facebook Login Error : ${error!!.localizedMessage}",context!!)
                Log.e(Constants.LOG_TAG,"Facebook login Exception "+error!!.localizedMessage)
            }

        })

    }



    private fun configureGoogleSiginIn() {
        Log.d(Constants.LOG_TAG,"Google Sign In Init")
        try{
            var gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()
             mGoogleSignInClient = GoogleSignIn.getClient(context!!,gso)
        }
        catch (ae : Exception){
            Log.d(Constants.LOG_TAG,"Google Sign In Exception : ${ae.localizedMessage}")
        }

    }


    private fun showProgress(status : Boolean){
        showLoadingProgress(progressBar,status)
    }

    private fun injectDependency() {
        val fragmentComponent = DaggerFragmentComponent.builder()
            .fragmentModule(FragmentModule())
            .sharedPreferenceModule(SharedPreferenceModule())
            .contextModule(ContextModule(context!!))
            .build()
        fragmentComponent.inject(this)

    }


    companion object {
        const val TAG = "LoginFragment"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginActivity = activity as LoginActivity
        injectDependency()
        configureFacebookSignIn()
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
    }

    private fun initUI() {
        tv_register.setOnClickListener {
            loginActivity.showRegisterFragment()
        }
        tv_login.setOnClickListener {
            checkLogin()
        }
        btn_google_sign_in.setOnClickListener {
            signInWithGoogle()
        }

        loginButton = rootView.findViewById<LoginButton>(R.id.login_button)
        loginButton.setReadPermissions(Arrays.asList("email", "public_profile"));
        loginButton.fragment = this




        tv_forgot_password.setOnClickListener {
            showForgotPasswordDialog()
        }


    }

    private fun showForgotPasswordDialog() {
        val dialog = Dialog(context!!,android.R.style.Theme_Black_NoTitleBar_Fullscreen)
        dialog .requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog .setCancelable(false)
        dialog .setContentView(R.layout.dialog_forgot_password)
        val tvSubmit = dialog .findViewById(R.id.tv_submit) as TextView
        val tvClose = dialog .findViewById(R.id.tv_close) as TextView
        val edEmail = dialog.findViewById(R.id.ed_email) as TextView
        tvSubmit.setOnClickListener {
            submitResetEmail(edEmail.text.toString(),dialog)
        }
        tvClose.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun submitResetEmail(emailReset: String,dialog: Dialog) {
        if(emailReset.isNullOrEmpty())
        {
            showShortToast("Please enter email id",context!!)
            return
        }
        var forgotPasswordObservable = api.forgotPassword(emailReset)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if(it.status == 200)
                {
                    showShortToast("Reset password link sent to email",context!!)
                    dialog.dismiss()
                }
                else{
                    showShortToast("Unable to process the request please try again",context!!)
                }

            },{
                showShortToast("Unable to process the request please try again",context!!)
            })
        disposable.add(forgotPasswordObservable)

    }

    private fun signInWithGoogle() {
        if(!::mGoogleSignInClient.isInitialized)
            return

        var signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent,RC_GOOGLE_SIGN_IN)
    }

    fun storeUserData(loginUserModel: LoginUserModel){
        mySharedPreferences.clearDatas()
        mySharedPreferences.putBoolean(Constants.IS_LOGGED,true)
//        mySharedPreferences.putString(Constants.USER_NAME,newUserRegisterModel.data.name)
//        mySharedPreferences.putString(Constants.USER_CONTACT,newUserRegisterModel.data.contact)
        mySharedPreferences.putString(Constants.USER_EMAIL,mUserName)
        mySharedPreferences.putString(Constants.USER_TOKEN,loginUserModel.token)
        mySharedPreferences.putIntData(Constants.USER_ID,loginUserModel.userid)


        goToMainActivity(context!!)
    }

    private fun checkLogin() {
        mUserName = ed_user_name.text.toString()
        mPassword = ed_password.text.toString()
            if(mUserName.isEmpty() || mPassword.isEmpty())
            {
                showShortToast("Please enter valid username and password",context!!)
            }
            else
            {
                showProgress(true)
                var loginObservable = api.loginCheck(mUserName,mPassword)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        showProgress(false)
                        Log.d(Constants.LOG_TAG,it.toString())
                        if(it.errors.isNullOrEmpty())
                        {
                            if(it.status==200)
                                storeUserData(it)
                            else
                                showShortToast(it.msg,context!!)
                        }
                        else
                        {
                            toastErrorMessage(context!!,it.errors)
                        }

                    },{
                        showProgress(false)
                        showShortToast(it.localizedMessage,context!!)
                        Log.d(Constants.LOG_TAG,it.localizedMessage)
                    })
                disposable.add(loginObservable)
            }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == RC_GOOGLE_SIGN_IN){
            var task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleGoogleSignInResult(task)
        }
        else
        {
            callBackManager.onActivityResult(requestCode, resultCode, data);
            Log.d(Constants.LOG_TAG,"Facebook Call back")

        }
    }

    private fun handleGoogleSignInResult(task: Task<GoogleSignInAccount>?) {
        try{
            var account = task!!.getResult(ApiException::class.java)
            updateUI(account)
            Log.d(Constants.LOG_TAG,"Success Google Sign IN "+account!!.idToken)
        }
        catch (ae : ApiException)
        {
            Log.e(Constants.LOG_TAG, "signInResult:failed code=" + ae.getStatusCode()+" -- "+ae.localizedMessage);
        }
    }

    private fun updateUI(account: GoogleSignInAccount?) {
        Log.d(Constants.LOG_TAG,account!!.displayName)
        mUserName = account.displayName.toString()
        mEmail = account.email.toString()
        mGoogleToken = account.idToken.toString()
        Log.d(Constants.LOG_TAG,"GMAIL TOKEN "+account.id)
        submitRegisterDatas()

    }

    fun submitRegisterDatas(){
        showProgress(true)



        var regObservable = api.gmailLoginCheck(mEmail,mUserName,mGoogleToken)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                showProgress(false)
                if(it.status == 200)
                {
                    storeUserData(it)
                }
                else{
                    showShortToast(it.msg.toString(),context!!)
                }


            },{
                showProgress(false)
                Log.d(Constants.LOG_TAG,it.localizedMessage)
            })

        disposable.add(regObservable)


    }

    fun storeUserData(googleLoginModel: GoogleLoginModel){
        mySharedPreferences.clearDatas()
        mySharedPreferences.putBoolean(Constants.IS_LOGGED,true)
        mySharedPreferences.putString(Constants.USER_NAME,mUserName)
      //  mySharedPreferences.putString(Constants.USER_CONTACT,newUserRegisterModel.data.contact)
        mySharedPreferences.putString(Constants.USER_EMAIL,mEmail)
        mySharedPreferences.putString(Constants.USER_TOKEN,googleLoginModel.token)
        mySharedPreferences.putIntData(Constants.USER_ID,googleLoginModel.userid)
        mySharedPreferences.putBoolean(Constants.IS_SOCIAL_LOGIN,true)
        goToMainActivity(context!!)

    }


}
